<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'Woo');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', '127.0.0.1');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'wwQB8QN028b.:PJ:1$i:@3hSa+sIdB6j}tLd:SR?-zfvePW?Nb-qC,)r=W5Q(q:!');
define('SECURE_AUTH_KEY',  'nd8.P|HPYm*K9{#*6`$|xC#Pvl =:Naa=E5e_L^ f;8o<.BwNi+/,bS^+e#ZHamZ');
define('LOGGED_IN_KEY',    'xO zyGcKI;.oT@o,*7&p=PS5? BzXo<I;r; =M4h4LnH!<siqEo$4HB%K.*#wXyy');
define('NONCE_KEY',        'nmW(1D?T,VyAZlnOR[yx`v|=B|b<i1+x?68;q7XpDp&*#7=7;N PuZ=^Yi}EW9L@');
define('AUTH_SALT',        'hs1(0ANv~o`6EKb`3(_gO3qK,zFt]r-=SIp{M1#:aVwO+{/JyDJpv(A%FBL{}AF5');
define('SECURE_AUTH_SALT', ':x1z?.#s?*C$j%D9zG`kft8LlIK.i{oY4~@H@&r6TBfoN`%1mN2q`C-R`~`B>th%');
define('LOGGED_IN_SALT',   '1S]k8Ost jO&gC=2O2PmwY{S<dtZ}-{ft$byAsH~[uTSZd7%tW?zWs*{d @/ t|P');
define('NONCE_SALT',       'BOnNy-C0>&B|:``R^cN9ZI8$,raGH9rcqa$y>J2x&im=pX8/~r*eX|I~kj254,fo');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');

<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

get_header(); ?>

<?php 
    global $product;
    wc_print_notices(); 
    do_action( 'woocommerce_before_cart' ); 
?>

<form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">    

<!-- begin container  -->
        <div class="content container">

            <!-- begin breadcrumbs  -->
            <div class="breadcrumbs">
                <ul>
                    <li><a href="">Главная</a></li>
                    <li>Корзина</li>
                </ul>
            </div>
            <!-- end breadcrumbs -->

            <h1 class="title"><span>Корзина</span></h1>

            <div class="cart">
                 <?php do_action( 'woocommerce_before_cart_contents' ); ?>

                <?php
                    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                        $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                    $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

                    if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
                        $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
                ?>
                <div class="wrap-cart__items">
                    <div class="cart__item">
                        <div class="cart__item-col cart__wallpapers">
                            <<?php
                                $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

                                if ( ! $product_permalink ) {
                                    echo $thumbnail;
                                } else {
                                    printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail );
                                }
                            ?>
                        </div>
                        <div class="cart__item-col cart__names">
                            <div class="cart__name">
                                <?php
                                if ( ! $product_permalink ) {
                                    echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;';
                                } else {
                                    echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key );
                                }

                                // Meta data
                                echo WC()->cart->get_item_data( $cart_item );

                                // Backorder notification
                                if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
                                    echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>';
                                }
                            ?>
                            </div>
                            <div class="cart__article">
                                Артикул: <?php global $product; echo $_product->get_sku(); ?>
                            </div>
                        </div>
                        <div class="cart__item-col cart__quantity quantity">
                            <div class="wrap-quantity">
                                <?php
                                if ( $_product->is_sold_individually() ) {
                                    $product_quantity = sprintf( '1 <input type="hidden" class="cart__quantity-field" name="cart[%s][qty]" value="1" />', $cart_item_key );
                                } else {
                                    $product_quantity = woocommerce_quantity_input( array(
                                        'input_name'  => "cart[{$cart_item_key}][qty]",
                                        'input_value' => $cart_item['quantity'],
                                        'max_value'   => $_product->get_max_purchase_quantity(),
                                        'min_value'   => '0',
                                    ), $_product, false );
                                }

                                echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
                            ?>
                            </div>
                        </div>
                        <div class="cart__item-col cart__price">
                            <div class="cart-price"><?php
                                echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
                            ?></div>
                            <div class="cart-price_old">1 шт. = <?php
                                echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
                            ?></div>
                        </div>
                        <div class="cart__item-col cart__delete">
                            <?php
                                 echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
                                        '<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s"><button class="cart__delete-btn btn_blue" type="button"><span>delete product</span></button></a>',
                                    esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
                                    __( 'Remove this item', 'woocommerce' ),
                                    esc_attr( $product_id ),
                                    esc_attr( $_product->get_sku() )
                                ), $cart_item_key );
                            ?>       
                        </div>
                    </div>
                            <?php
                        }
                    }
                ?>

                <div class="cart-footer">
                    <div class="cart-footer__item cart-footer__item_btn">
                        <input type="submit" class="" name="update_cart" value="Продолжить покупки">
                    </div>
                    <div class="cart-footer__item cart-footer__item_shipping">
                        <div class="cart-footer__item-title">ДОСТАВКА:</div>
                        <div class="cart-footer__item-price">$399</div>
                    </div>
                    <div class="cart-footer__item cart-footer__item_total">
                        <div class="cart-footer__item-title">ИТОГО:</div>
                        <div class="cart-footer__item-price"><?php wc_cart_totals_subtotal_html(); ?></div>
                    </div>
                </div>
            </form>
            </div>

            <?php
            wc_print_notices();

            do_action( 'woocommerce_before_checkout_form', $checkout );

            // If checkout registration is disabled and not logged in, the user cannot checkout
            /*
            if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
                echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
                return;
            }
            */
            ?>
            <form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data" novalidate="novalidate">
                    <?php //if ( $checkout->get_checkout_fields() ) : ?>

                <?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>
                <div class="cart__footer-col cart__checkout">
                    <div class="cart__title cart__title_checkout">оформление ЗАКАЗА</div>
                    <?php do_action( 'woocommerce_checkout_billing' ); ?>
                    <?php //do_action( 'woocommerce_checkout_shipping' ); ?>
                </div>
                <?php //do_action( 'woocommerce_checkout_before_order_review' ); ?>
                <?php //do_action( 'woocommerce_checkout_billing' ); ?>
                    <?php do_action( 'woocommerce_checkout_shipping' ); ?>
                    
                    <?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>
                    <?php do_action( 'woocommerce_checkout_order_review' ); ?>
                </form>
                <?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>

            </div>
        </div>
          
        <? echo do_shortcode('[woocommerce_checkout]'); ?>
        <!-- end container -->      

<?php get_footer(); ?>
<?php

function boats_setup(){
	load_theme_textdomain('boats');
	add_theme_support('title-tag');
	add_theme_support('custom-logo', array(
		'flex-height' => true, 
	));
	add_theme_support('post-thumbnails');

}
add_action( 'after_setup_theme', 'boats_setup' );


function fleet_styles() {
    wp_enqueue_style( 'admin.css', get_stylesheet_directory_uri() . '/css/admin.css' );
    wp_enqueue_style( 'vendors.min',    get_stylesheet_directory_uri() . '/css/vendors.min.css' );
    wp_enqueue_style( 'main.min',    get_stylesheet_directory_uri() . '/css/main.min.css' );
}
add_action( 'wp_enqueue_scripts', 'fleet_styles' );

function fleet_scripts() {
 	wp_register_script( 'vendors.min', get_template_directory_uri() . '/js/vendors.min.js' );
    wp_register_script( 'main.min', get_template_directory_uri() . '/js/main.min.js' );
    wp_register_script( 'select2.full.min', get_template_directory_uri() . '/js/select2.full.min.js' );
     
    wp_enqueue_script( 'vendors.min' );
    wp_enqueue_script( 'main.min' );
    wp_enqueue_script( 'select2.full.min' );
    wp_enqueue_script( 'jquery');
}
add_action( 'wp_enqueue_scripts', 'fleet_scripts' );


if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();	
}

// WooCommerce products and categories/subcategories separately in archive pages
function mynew_product_subcategories( $args = array() ) {
	$parentid = get_queried_object_id();
	$args = array(
	    'parent' => $parentid
	);
	$terms = get_terms( 'product_cat', $args );
	if ( $terms ) {   
	        foreach ( $terms as $term ) {              
	            echo '<div class="wallpapers__item action">';                        
	                woocommerce_subcategory_thumbnail( $term ); 
	                echo '<div class="wallpapers__info">';
	                	echo '<div class="wallpapers__info-item wallpapers__title">';
	                		echo $term->name;
	                	echo '</div>';
	                    echo '<div class="wallpapers__info-item">';
	                        echo '<a class="wallpapers__link" href="'.  esc_url( get_term_link( $term ) ) .'">Посмотреть</a>';
	                    echo '</div>';
				echo '</div>';
           	echo '</div>';
	                    //echo '<a href="'  '" class="' . $term->slug . '">';                                                    
	    }
	}
}
add_action( 'woocommerce_before_shop_loop', 'mynew_product_subcategories', 50 );

// WooCommerce products and categories/subcategories separately in archive pages
function mynew_woocommerce_sidebar( $args = array() ) {
	$parentid = get_queried_object_id();
			$args = array(
			    'parent' => $parentid
			);
			$terms = get_terms( 'product_cat', $args );
			if ( $terms ) {   
				 echo '<div class="sidebar sidebar-menu">';
		                echo '<h4 class="sidebar-menu__title">ФОТООБОИ</h4>';
		                echo '<ul>';

			        foreach ( $terms as $term ) {                 
			            	echo '<li><a href="'.  esc_url( get_term_link( $term ) ) .'">'.$term->name.'</a></li>';      
			            }
			            echo '</ul>';
		        echo '</div>';
			}
}
add_action( 'woocommerce_sidebar', 'mynew_woocommerce_sidebar', 10 );

function wallpapers_register_wp_sidebars() {
 
    /* В боковой колонке - первый сайдбар */
    register_sidebar(
        array(
            'id' => 'true_side', // уникальный id
            'name' => 'Боковая колонка', // название сайдбара
            'description' => 'Перетащите сюда виджеты, чтобы добавить их в сайдбар.', // описание
            'before_widget' => '<div id="%1$s" class="side widget %2$s">', // по умолчанию виджеты выводятся <li>-списком
            'after_widget' => '</div>',
            'before_title' => '<h4 class="sidebar-menu__title">', // по умолчанию заголовки виджетов в <h2>
            'after_title' => '</h4>'
        )
    );
 
    /* В подвале - второй сайдбар */
    register_sidebar(
        array(
            'id' => 'true_foot',
            'name' => 'Футер',
            'description' => 'Перетащите сюда виджеты, чтобы добавить их в футер.',
            'before_widget' => '<div id="%1$s" class="foot widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>'
        )
    );
}
 
add_action( 'widgets_init', 'wallpapers_register_wp_sidebars' );


add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {
     unset($fields['shipping']['shipping_first_name']);
     unset($fields['shipping']['shipping_company']);
     unset($fields['shipping']['shipping_address_1']);
     unset($fields['shipping']['shipping_address_2']);
     unset($fields['shipping']['shipping_city']);
     unset($fields['shipping']['shipping_postcode']);
     unset($fields['shipping']['shipping_country']);
     unset($fields['shipping']['shipping_state']);
     unset($fields['shipping']['shipping_last_name']);
     unset($fields['shipping']['shipping_phone']);
     //unset($fields['order']['order_comments']);
     

     unset($fields['billing']['billing_first_name']);
     unset($fields['billing']['billing_company']);
     unset($fields['billing']['billing_address_1']);
     unset($fields['billing']['billing_address_2']);
     unset($fields['billing']['billing_city']);
     unset($fields['billing']['billing_postcode']);
     unset($fields['billing']['billing_country']);
     unset($fields['billing']['billing_state']);
     unset($fields['billing']['billing_last_name']);
     unset($fields['billing']['billing_phone']);
     unset($fields['billing']['billing_email']);
     //echo $a = '<p>строка</p>';
      $fields['billing']['billing_first_name'] = array(
      	'required' => true,
        );
     $fields['billing']['billing_first_name'] = array(
        'type'          => 'text',
        'class'         => array('my-field-class form-row-wide'),
        'label'         => ('Имя*'),
        'placeholder'   => ('Введите Имя'),
        'required' => true,
        );
     $fields['billing']['billing_last_name'] = array(
        'type'          => 'text',
        'class'         => array('my-field-class form-row-wide'),
        'label'         => ('Фамилия*'),
        'placeholder'   => ('Введите Фамилию'),
        'required' => true,
        );
     $fields['billing']['billing_phone'] = array(
        'type'          => 'text',
        'class'         => array('my-field-class form-row-wide'),
        'label'         => ('Телефон*'),
        'placeholder'   => ('Введите телефон'),
        'required' => true,
        );
     $fields['billing']['billing_email'] = array(
        'type'          => 'text',
        'class'         => array('my-field-class form-row-wide'),
        'label'         => ('Электронная почта*'),
        'placeholder'   => ('Введите электронную почту'),
        'required' => true,
        );
      $fields['shipping']['shipping_street'] = array(
        'type'          => 'text',
        'class'         => array('my-field-class form-row-wide'),
        'label'         => ('Улица'),
        'placeholder'   => ('Введите улицу'),
        );
      $fields['shipping']['shipping_home'] = array(
        'type'          => 'text',
        'class'         => array('my-field-class form-row-wide'),
        'label'         => ('Дом'),
        'placeholder'   => ('Введите дом'),
        );
      $fields['shipping']['shipping_home_number'] = array(
        'type'          => 'text',
        'class'         => array('my-field-class form-row-wide'),
        'label'         => ('Квартира'),
        'placeholder'   => ('Введите номер квартиры'),
        );
     /*
     $fields['billing']['patronymic_name'] = array(
        'type'          => 'text',
        'class'         => array('my-field-class form-row-wide'),
        'label'         => ('Patronymic Name'),
        'placeholder'   => ('Enter Patronymic'),
        );
        */
     return $fields;
}

/**
 * Add the field to the checkout
 */
//add_action( 'woocommerce_after_order_notes', 'my_custom_checkout_field' );

function my_custom_checkout_field( $checkout ) {
    woocommerce_form_field( 'patronymic_name', array(
        'type'          => 'text',
        'class'         => array('my-field-class form-row-wide'),
        'label'         => ('Patronymic Name'),
        'placeholder'   => ('Enter Patronymic'),
        ), $checkout->get_value( 'patronymic_name' ));

    echo '</div>';

}

?>
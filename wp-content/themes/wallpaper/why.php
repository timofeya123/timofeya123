        <!-- begin why  -->
        <div class="why">
            <div class="container">
                <div class="why__item">
                    Почему <br>
                    наши фотообои <br>
                    лучшие
                </div>
                <?php
                    for ($i=1; $i <6 ; $i++) { 
                ?>
                <div class="why__item">
                    <div class="why__col">
                        <? $image = get_field('why_image_'.$i, 'option');
                            if( !empty($image) ): ?>

                               <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

                        <?php endif; ?>
                    </div>
                    <div class="why__col">
                        <div class="why__title">
                            <? $value = get_field( "why_name_$i", 'option' );

                            if( $value ) {
                                                
                                echo $value;

                            } else {

                                echo 'none';
                                                
                            } ?>
                                                
                        </div>
                        <div class="why__content">
                            <? $value = get_field( "why_text_$i", 'option' );

                            if( $value ) {
                                                
                                echo $value;

                            } else {

                                echo 'none';
                                                
                            } ?>
                        </div>
                    </div>
                </div>
                <? } ?>
            </div>
        </div>
        <!-- end why -->

        <!-- begin info  -->
        <div class="footer-info">
            <div class="container">
                <p>Не нашли, что искали? <br>
                    Может быть вы просто что-то пропустили. <br>
                    Посмотрите наш каталог еще раз. </p>
                <a class="footer-info__link" href="http://wordultra/shop/">Вернуться в каталог</a>
            </div>
        </div>
        <!-- end info -->
<div class="footer__item social-menu">
	<span class="social-menu__title"> 
		<? $value = get_field( "social_title", 'option' );
            if( $value ) {
                echo $value;
            } else {
            	echo 'none';
            } ?>
    </span>
	<ul>
		<?php
            for ($i=1; $i <6 ; $i++) { 
        ?>
	    <li><a href="" target="_blank">
	    	<img src="" alt="">
	    	<? $image = get_field('social_image_'.$i, 'option');
            if( !empty($image) ): ?>

            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

            <?php endif; ?>
	    </a></li>
	    <? } ?>
	</ul>
</div>


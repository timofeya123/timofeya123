<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<title>Site wallpaper</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
	</head>
	<body>
	<header>
		<!-- begin header-top  -->
		<div class="header-top js-header-top">
		    <div class="container">
		        <div class="header-top__item" style="float: left;">
		            <a class="header-top__city header__link" href="">Омск</a>
		            <a class="header-top__address header__link" href="">ул. Гагарина, 8/2</a>
		            <a class="header-top__cart header__link" href="">Корзина (3)</a>
		            <div class="header-top-menu">
		                <ul>
		                    <li class="current-menu-item"><a>АКЦИИ</a></li>
		                    <li><a href="">ФОТООБОИ</a></li>
		                    <li><a href="">ОПЛАТА и ДОСТАВКА</a></li>
		                </ul>
		            </div>
		        </div>
		        <div class="header-top__item" style="float: right;">
		            <button class="header-top__phone-call" type="button">Заказать обратный звонок</button>
		            <a class="header-top__number-phone" href="tel:+89087997555">8 <span>908</span> 799-75-55</a>
		        </div>
		    </div>
		</div>
		<!-- end header-top -->

		<!-- begin header  -->
		<div class="header container">
		    <div class="header__item header__logo">
		        <a href="" class="header__logo-link">
		            <img src="images/logo.png" alt="Топ фотообои">
		        </a>
		    </div>
		    <div class="header__item header__menu">
		        <ul>
		            <li class="header__menu_stock"><a href="">Акции</a></li>
		            <li class="current-menu-item header__menu_wallpapers"><a href="">Фотообои</a></li>
		            <li class="header__menu_payment-shipping"><a href="">Оплата и доставка</a></li>
		        </ul>
		    </div>
		</div>
		<!-- end header -->	
	</header>
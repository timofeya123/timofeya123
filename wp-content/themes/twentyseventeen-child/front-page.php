<?php
get_header();
?>

<?php 
get_search_form();
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); 
		?>
		<br>
		<div style="post">
			<div style="title">
				<h2>
		<?
		the_title();
		?>
				</h2>		
			</div>
			<div style="date">
				<i>
		<?
		the_date();
		?>
				</i>		
			</div>
			<div style="author">
				<b>
		<?
		the_author();
		?>
				</b>		
			</div>
			<div style="excerpt">
				<p>
		<?
		the_excerpt();
		?>
				</p>		
			</div>
		</div>
		<hr />
		<?
		//
		// Post Content here
		//
	} // end while
} // end if
?>

<?php
get_footer();
?>
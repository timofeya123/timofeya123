<?php
get_header();
?>

	<h1>Все посты из Custom Post Type</h1>

<?php 
	
	


	if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); 
		?>
		<br>
		<div class="post">
			<div class="title">
				<h2>
		<?
		the_title();
		?>
				</h2>		
			</div>
			<div class="image" style="padding: 10px;">
		<?
		the_post_thumbnail();
		?>		
			</div>
			<div class="date cat" style="float: left; padding: 10px;">
				Категории: <i>
		<?
		//the_date(); 
		the_terms($post->ID, 'Brand');
		?>
				</i>		
			</div>
			<div class="meta" style="float: left; padding: 10px;">
				Метки: <i>
					<?
		the_terms($post->ID, 'Color');
		?>
				</i>		
			</div>
			<div class="author" style="padding: 10px;">
				<b>
					Автор: 
		<?
		the_author();
		?>
				</b>		
			</div>
			<div class="excerpt">
				<p>
		<?
		the_excerpt();
		?>
				</p>		
			</div>
		</div>
		<hr />
		<p>
			Владелец: 
		<?
		$value = get_field( "owner_name" );

		if( $value ) {
		    
		    echo $value;

		} else {

		    echo 'empty';
		    
		}
		?>
		</p>
		<p>
			Телефон:
		<?
		$value = get_field( "owner_phone" );

		if( $value ) {
		    
		    echo $value;

		} else {

		    echo 'empty';
		    
		}
		?>
		</p>
		<p>
			<table style="width:400px; border:5px solid black; text-align: center;">
				<tr style="width:400px; border:5px solid black;">
					<td style="border:5px solid black;">
					Название свойства	
					</td><td>
					Название значения свойства	
					</td>
				</tr>
		<?
		if(have_rows('car_prop')){
			while (have_rows('car_prop')) 
			{ 
				?>
				<tr>
					<td style="border:1px solid black;">
				<?
				the_row();
				the_sub_field('prop_name');
				?>
					</td><td style="border:1px solid black;">
				<?
				the_sub_field('prop_name_');
				?>
					</td>
				</tr>
				<?
			}
		}
		else{
			echo 'empty';
		}
		?>
			</table>
		</p>
		<p>
		<?
		
		$images = get_field('gallery');
		$size = 'full';

		if ($images){
			?>
				<ul>
				<?
			foreach ($images as $image) {
				?>
				<li style="float: left; width: 150px;">
				<?
				echo wp_get_attachment_image($image['ID'], $size);
				?>
				</li>
				<?
			}
			?>
				</usl>
				<?
		}
		
		?>
				</p>
		<?
		//
		// Post Content here
		//
	} // end while
	//wp_reset_postdate();
} // end if
?>

<?php
get_footer();
?>
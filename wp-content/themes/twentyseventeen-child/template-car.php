<?
/*
Template Name: Car
*/
?>

<?php
get_header();
?>

	<h1>Все посты из Custom Post Type</h1>

<?php 
	$args = array(
		'post_type' => 'car',
		'publish' => true,
		'paged' => get_query_var('paged'),
	);
	//query_post($args);
	
	$tt = new WP_Query($args);



	if ( $tt->have_posts() ) {
	while ( $tt->have_posts() ) {
		$tt->the_post(); 
		?>
		<br>
		<div class="post">
			<div class="title">
				<h2>
		<?
		the_title();
		?>
				</h2>		
			</div>
			<div class="date cat" style="float: left; padding: 10px;">
				<i>
		<?
		//the_date(); 
		the_terms($post->ID, 'Brand');
		?>
				</i>		
			</div>
			<div class="meta" style="float: left; padding: 10px;">
				<i>
					<?
		the_terms($post->ID, 'Color');
		?>
				</i>		
			</div>
			<div class="author" style="padding: 10px;">
				<b>
		<?
		the_author();
		?>
				</b>		
			</div>
			<div class="excerpt">
				<p>
		<?
		the_excerpt();
		?>
				</p>		
			</div>
		</div>
		<hr />
		<?
		//
		// Post Content here
		//
	} // end while
	//wp_reset_postdate();
} // end if
?>

<?php
get_footer();
?>
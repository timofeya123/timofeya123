<?php
/*
Пособие по созданию архива для вывода записей по терминам таксономии
Шаблон архива для записей пользовательского типа car
?>
 
<?php get_header(); ?>
 
    <div id="main-content" class="main-content">
 
    <div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">
 
            <header class="archive-header">
                <h1 class="archive-title">
                    <?php post_type_archive_title(); ?>
                </h1>
            </header><!-- .archive-header -->
            
            <?php //начало выборки терминов для таксономии brand
			 $terms = get_terms( 'brand', array(
			    'orderby'    => 'count',
			    'hide_empty' => 0
			 ) );

			  foreach( $terms as $term ) {
 
				    // Определение запроса
				    $args = array(
				        'post_type' => 'car',
				        'animal_cat' => $term->slug
				    );
				    $query = new WP_Query( $args );
			     
			     // вывод названий записей в тегах заголовков
			     echo'<h2>' . $term->name . '</h2>';
			     
			    // вывод списком заголовков записей
			    echo '<ul>';
			     
			        // Начало цикла
			        while ( $query->have_posts() ) : $query->the_post(); ?>
			 
			        <li class="car-listing" id="post-<?php the_ID(); ?>">
			            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			        </li>
			         
			        <?php endwhile;
			     
			    echo '</ul>';
			     
			    // используем сброс данных записи, чтобы восстановить оригинальный запрос
			    wp_reset_postdata();
			 
 				}
			 ?> 
                         
        </div><!-- #content -->
    </div><!-- #primary -->
    <?php get_sidebar( 'content' ); ?>
</div><!-- #main-content -->
 
<?php
get_sidebar();
get_footer();*/

?>

<?php
get_header();
?>

	<h1>Все посты из Custom Post Type</h1>

<?php 



	if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); 
		?>
		<br>
		<div class="post">
			<div class="title">
				<h2>
		<?
		the_title();
		?>
				</h2>		
			</div>
			<div class="date cat" style="float: left; padding: 10px;">
				<i>
		<?
		//the_date(); 
		the_terms($post->ID, 'Brand');
		?>
				</i>		
			</div>
			<div class="meta" style="float: left; padding: 10px;">
				<i>
					<?
		//the_terms($post->ID, 'Color');
		?>
				</i>		
			</div>
			<div class="author" style="padding: 10px;">
				<b>
		<?
		the_author();
		?>
				</b>		
			</div>
			<div class="excerpt">
				<p>
		<?
		the_excerpt();
		?>
				</p>		
			</div>
		</div>
		<hr />
		<?
		//
		// Post Content here
		//
	} // end while
	//wp_reset_postdate();
} // end if
?>

<?php
get_footer();
?>
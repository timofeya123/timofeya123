<?php
function my_theme_enqueue_styles() {

    $parent_style = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

/*
function create_post_type() {
  register_post_type( 'Car',
    array(
      'labels' => array(
        'name' => __( 'Cars' ),
        'singular_name' => __( 'Car' ),
      ),
      'public' => true,
      'has_archive' => true
    )
  );
}
add_action( 'init', 'create_post_type' );

function brand_init() {
	// create a new taxonomy
	register_taxonomy(
		'Brand',
		array('car'),
		array(
			'hierarchical' => true,
			'labels' => array(
			'name' => 'Brand',
			'singular_name' => 'Brand',
			'all_items' => 'all_items',
			'edit_item' => 'edit_items',
			'add_new_item' => "add_new_item"
			), 
			'show_ui' => true,
			'rewrite' => array( 'slug' => 'Brand' ),
			'capabilities' => array(
				'assign_terms' => 'edit_guides',
				'edit_terms' => 'publish_guides'
			),
			'show_admin_column' => true
		)
	);
}
add_action( 'init', 'brand_init');

function color_init() {
	// create a new taxonomy
	register_taxonomy(
		'Color',
		'car',
		array(
			'label' => __( 'Color' ),
			'rewrite' => array( 'slug' => 'palitra' ),
			'capabilities' => array(
				'assign_terms' => 'edit_guides',
				'edit_terms' => 'publish_guides'
			)
		)
	);
}
add_action( 'init', 'color_init' );

/*
function people_init() {
	// create a new taxonomy
	register_taxonomy(
		'people',
		'post',
		array(
			'label' => __( 'People' ),
			'rewrite' => array( 'slug' => 'person' ),
			'capabilities' => array(
				'assign_terms' => 'edit_guides',
				'edit_terms' => 'publish_guides'
			)
		)
	);
}
add_action( 'init', 'people_init' );

wp_set_object_terms( 123, 'Bob', 'person' );

 $terms = get_terms( array(
                          'taxonomy' => 'your_custom_taxonomy',
                          'hide_empty' => false,  ) );

 $output = '';
 foreach($terms as $term){
    $output .= '<input type="checkbox" name="terms" value="' . $term->name . '" /> ' .  $term->name . '<br />';
  }

$query = new WP_Query( array( 'person' => 'bob' ) );

$args = array(
	'tax_query' => array(
		array(
			'taxonomy' => 'person',
			'field' => 'slug',
			'terms' => 'bob'
		)
	)
);
$query = new WP_Query( $args );

function create_post_type() {
  register_post_type( 'Hotel',
    array(
      'label' => 'Hotel',
      'public' => true,
      'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
      'taxonomies' => array( 'Col')
    )
  );
}
add_action( 'init', 'create_post_type' );

function color_init() {
	// create a new taxonomy
	register_taxonomy(
		'Col',
		'Hotel',
		array(
			'public' => true,
			'label' => __( 'Colory' ),
			'rewrite' => array( 'slug' => 'Col' ),
			'hierarchical' => true
			
		)
	);
}
add_action( 'init', 'color_init' );
*/

add_action('init', 'register_post_types');
function register_post_types(){
	register_post_type('Car', array(
		'label'  => null,
		'labels' => array(
			'name'               => 'Car', // основное название для типа записи
			'singular_name'      => 'Car', // название для одной записи этого типа
			'add_new'            => 'Добавить Car', // для добавления новой записи
			'add_new_item'       => 'Добавление Car', // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => 'Редактирование Car', // для редактирования типа записи
			'new_item'           => 'Новое Car', // текст новой записи
			'view_item'          => 'Смотреть Car', // для просмотра записи этого типа.
			'search_items'       => 'Искать Car', // для поиска по этим типам записи
			'not_found'          => 'Не найдено', // если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
			'parent_item_colon'  => '', // для родителей (у древовидных типов)
			'menu_name'          => 'Car', // название меню
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => null,
		'exclude_from_search' => null,
		'show_ui'             => null,
		'show_in_menu'        => null, // показывать ли в меню адмнки
		'show_in_admin_bar'   => null, // по умолчанию значение show_in_menu
		'show_in_nav_menus'   => null,
		'show_in_rest'        => null, // добавить в REST API. C WP 4.7
		'rest_base'           => null, // $post_type. C WP 4.7
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-post-status', 
		//'capability_type'   => 'post',
		//'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
		//'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
		'hierarchical'        => true,
		'supports'            => array('title','editor','thumbnail'), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'          => array(),
		//'has_archive'         => false,
		'rewrite'             => true,
		'has_archive'		  => true,
		'query_var'           => true,
	) );
}

// хук для регистрации
add_action('init', 'create_taxonomy');
function create_taxonomy(){
	// список параметров: http://wp-kama.ru/function/get_taxonomy_labels
	register_taxonomy('Brand', array('car'), array(
		'label'                 => '', // определяется параметром $labels->name
		'labels'                => array(
			'name'              => 'Brands',
			'singular_name'     => 'Brand',
			'search_items'      => 'Search Brands',
			'all_items'         => 'All Brands',
			'view_item '        => 'View Brand',
			'parent_item'       => 'Parent Brand',
			'parent_item_colon' => 'Parent Brand:',
			'edit_item'         => 'Edit Brand',
			'update_item'       => 'Update Brand',
			'add_new_item'      => 'Add New Brand',
			'new_item_name'     => 'New Brand Name',
			'menu_name'         => 'Brand',
		),
		'description'           => '', // описание таксономии
		'public'                => true,
		'publicly_queryable'    => null, // равен аргументу public
		'show_in_nav_menus'     => true, // равен аргументу public
		'show_ui'               => true, // равен аргументу public
		'show_tagcloud'         => true, // равен аргументу show_ui
		'show_in_rest'          => null, // добавить в REST API
		'rest_base'             => null, // $taxonomy
		'hierarchical'          => true,
		'update_count_callback' => '',
		'rewrite'               => true,
		//'query_var'             => $taxonomy, // название параметра запроса
		'capabilities'          => array(),
		'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
		'show_admin_column'     => true, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
		'_builtin'              => false,
		'show_in_quick_edit'    => null, // по умолчанию значение show_ui
	) );

	register_taxonomy('Color', 'car', array(
		'label'                 => '', // определяется параметром $labels->name
		'labels'                => array(
			'name'              => 'Colors',
			'singular_name'     => 'Color',
			'search_items'      => 'Search Colors',
			'all_items'         => 'All Colors',
			'view_item '        => 'View Color',
			'parent_item'       => 'Parent Color',
			'parent_item_colon' => 'Parent Color:',
			'edit_item'         => 'Edit Color',
			'update_item'       => 'Update Color',
			'add_new_item'      => 'Add New Color',
			'new_item_name'     => 'New Color Name',
			'menu_name'         => 'Color',
		),
		'description'           => '', // описание таксономии
		'public'                => true,
		'publicly_queryable'    => null, // равен аргументу public
		'show_in_nav_menus'     => true, // равен аргументу public
		'show_ui'               => true, // равен аргументу public
		'show_tagcloud'         => true, // равен аргументу show_ui
		'show_in_rest'          => null, // добавить в REST API
		'rest_base'             => null, // $taxonomy
		'hierarchical'          => false,
		'update_count_callback' => '',
		'rewrite'               => true,
		//'query_var'             => $taxonomy, // название параметра запроса
		'capabilities'          => array(),
		'meta_box_cb'           => 'post_tags_meta_box', // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
		'show_admin_column'     => true, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
		'_builtin'              => false,
		'show_in_quick_edit'    => null, // по умолчанию значение show_ui
	) );
}

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}

?>
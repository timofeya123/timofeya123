<?php if ( is_active_sidebar( 'true_side' ) ) : ?>


  <div class="sidebar sidebar-menu">
    <?php dynamic_sidebar( 'true_side' ); ?>
 
  </div>
 
<?php endif; ?>
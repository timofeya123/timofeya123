	

	<footer>
		<!-- begin footer  -->
		<div class="footer">
		    <div class="container">
		        <div class="footer__item footer__menu">
		            <ul>
		                <li><a href="">
		                	<? $value = get_field( "footer_seting", 'option' );

                            if( $value ) {
                                                
                                echo $value;

                            } else {

                                echo 'none';
                                                
                            } ?>
		                </a></li>
		                <li><a href="">
		                	<? $value = get_field( "footer_cont", 'option' );

                            if( $value ) {
                                                
                                echo $value;

                            } else {

                                echo 'none';
                                                
                            } ?>
		                </a></li>
		            </ul>
		        </div>
		        <div class="footer__item social-menu">
		       		<? include "social_menu.php"; ?>
		       </div>
		    </div>
		</div>
		<!-- end footer -->
	</footer>
</body>
<?php wp_footer(); ?>
</html>

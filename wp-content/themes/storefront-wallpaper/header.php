<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<title>Site wallpaper</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
	</head>
	<body>
	<header>
		<!-- begin header-top  -->
		<div class="header-top js-header-top">
		    <div class="container">
		        <div class="header-top__item" style="float: left;">
		            <a class="header-top__city header__link" href="">
		            	<? $value = get_field( "head-city", 'option' );

                            if( $value ) {
                                                
                                echo $value;

                            } else {

                                echo 'none';
                                                
                            } ?>
		            </a>
		            <a class="header-top__address header__link" href="">
		            	<? $value = get_field( "head-address", 'option' );

                            if( $value ) {
                                                
                                echo $value;

                            } else {

                                echo 'none';
                                                
                            } ?>
		            </a>
		            <a class="header-top__cart header__link" href="">
		            	<? $value = get_field( "head-cart", 'option' );

                            if( $value ) {
                                                
                                echo $value;

                            } else {

                                echo 'none';
                                                
                            } ?>
		            </a>
		            <? if( have_rows( 'head-top-menu', 'option' ) ) :	?>		
			            <div class="header-top-menu">
			                <ul>
			                	<? 	while ( have_rows( 'head-top-menu', 'option') ) : the_row(); ?>
			                    <li class="current-menu-item"><a><? the_sub_field('head-top-menu-text'); ?></a></li>
			                	<? endwhile; ?>
			                </ul>
			            </div>
		        	<? endif;?>
		        </div>
		        <div class="header-top__item" style="float: right;">
		            <button class="header-top__phone-call" type="button">
		            	<? $value = get_field( "head-button", 'option' );

                            if( $value ) {
                                                
                                echo $value;

                            } else {

                                echo 'none';
                                                
                            } ?>
		            </button>
		            <a class="header-top__number-phone" href="tel:+89087997555">
		            	<? $value = get_field( "head-tel", 'option' );

                            if( $value ) {
                                                
                                echo $value;

                            } else {

                                echo 'none';
                                                
                            } ?>
		            </a>
		        </div>
		    </div>
		</div>
		<!-- end header-top -->

		<!-- begin header  -->
		<div class="header container">
		    <div class="header__item header__logo">
		        <a href="" class="header__logo-link">
		        	<? $image = get_field('head-logo', 'option');
                            if( !empty($image) ): ?>

                               <img src="<?php echo $image['url']; ?>" alt="Топ фотообои" />

                        <?php endif; ?>
		        </a>
		    </div>
		    <? if( have_rows( 'head-top-menu', 'option' ) ) :	?>		
			    <div class="header__item header__menu">
			        <ul>
			        	<? 	while ( have_rows( 'head-top-menu', 'option') ) : the_row(); ?>
			        	<li class="<? the_sub_field('head-top-menu-class'); ?>"><a href="<? the_sub_field('head-top-menu-href'); ?>"><? the_sub_field('head-top-menu-text'); ?></a></li>
				       	<? endwhile; ?>
			        </ul>
			    </div>
		    <? endif; ?>
		</div>
		<!-- end header -->	
	</header>
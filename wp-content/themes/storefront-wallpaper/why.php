        <!-- begin why  -->
    <? if( have_rows( 'why_copy', 'option' ) ) :  ?>
        <div class="why">
            <div class="container">
                <?  while ( have_rows( 'why_copy', 'option') ) : the_row(); ?>
                    <? $image = get_sub_field('why-image');
                        if( !empty($image) ) { ?>
                            <div class="why__item">
                                <div class="why__col">
                                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                               </div>
                                <div class="why__col">
                                    <div class="why__title">
                                        <? the_sub_field('why-name'); ?>
                                    </div>
                                    <div class="why__content">
                                        <? the_sub_field('why-text'); ?>
                                    </div>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="why__item">
                                <? the_sub_field('why-name'); ?>
                            </div>
                        <? } ?>
                <? endwhile; ?>
            </div>
        </div>
    <? endif; ?>
        <!-- end why -->

        <!-- begin info  -->
        <div class="footer-info">
            <div class="container">
                <p>Не нашли, что искали? <br>
                    Может быть вы просто что-то пропустили. <br>
                    Посмотрите наш каталог еще раз. </p>
                <a class="footer-info__link" href="http://wordultra/shop/">Вернуться в каталог</a>
            </div>
        </div>
        <!-- end info -->
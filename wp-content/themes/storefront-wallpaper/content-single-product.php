<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>



<?php
	/**
	 * woocommerce_before_single_product hook.
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>

 <? //<div id="product-<?php the_ID(); >" <?php post_class(); ?> 

	<!-- begin product  -->
            <div class="product content">
                <div class="product__image">
                	<? the_post_thumbnail(); ?>
                	<?
						/**
						 * woocommerce_before_single_product_summary hook.
						 *
						 * @hooked woocommerce_template_loop_product_thumbnail - 10
						 */
						//do_action( 'woocommerce_before_single_product_summary' );
					?>
                	<?php
						/**
						 * woocommerce_before_single_product_summary hook.
						 *
						 * @hooked woocommerce_template_single_product_product_thumbnail - 10
						 */
						//do_action( 'woocommerce_before_single_product_summary' );
					?>
                </div>
                <div class="product__title">
               		<?php
						/**
						 * woocommerce_template_single_product_title_summary hook.
						 *
						 * @hooked woocommerce_template_single_product_title - 5
						 */
						do_action( 'woocommerce_template_single_product_title_summary' );
					?>
					<?php the_title(); ?>
            	</div>
            	<div class="product__container group">
                    <button class="btn btn_blue product__btn-like" type="button">Нравится этот товар</button>
                    <a class="btn product__btn-manual" href="">Инструкция по оклейке</a>
                    <div class="social-menu social-menu_product">
                    	<? include "social_menu.php"; ?>
                    </div>
                </div>
                <div class="product-items">
                    <div class="product__item action"> 
                    	<?php
							/**
							 * woocommerce_single_product_summary hook.
							 *
							 * @hooked woocommerce_template_single_title - 5
							 * @hooked woocommerce_template_single_rating - 10
							 * @hooked woocommerce_template_single_price - 10
							 * @hooked woocommerce_template_single_excerpt - 20
							 * @hooked woocommerce_template_single_add_to_cart - 30
							 * @hooked woocommerce_template_single_meta - 40
							 * @hooked woocommerce_template_single_sharing - 50
							 * @hooked woocommerce_template_single_product_attributes - 55
							 * @hooked WC_Structured_Data::generate_product_data() - 60
							 */
							//do_action( 'woocommerce_single_product_summary' );
						?>  <?php global $product; //echo ($product->get_attribute( 'size' )); ?>                      
                        <div class="product__article">Артикул: <?php  echo $product->get_sku(); ?></div>
                        	
                        <div class="product__size">Размер: <? echo $product->get_length(); ?>x<? echo $product->get_width(); ?>м </div>
                        <div class="product__number-bands">Количество полос: <? if ( $product->get_attribute( 'N' ) ) { echo ( $product->get_attribute( 'N' ) ); }  ?></div>
                        <div class="product__price"><? echo $product->get_price(); ?> $</div>

						<form class="cart" method="post" enctype='multipart/form-data' action="http://wordultra/cart">
                                    <?php
                                        /**
                                         * @since 2.1.0.
                                         */
                                        do_action( 'woocommerce_before_add_to_cart_button' );

                                        /**
                                         * @since 3.0.0.
                                         */
                                        do_action( 'woocommerce_before_add_to_cart_quantity' );

                                        woocommerce_quantity_input( array(
                                            'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
                                            'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
                                            'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : $product->get_min_purchase_quantity(),
                                        ) );
                                    ?>
                                   
                                   	<div style="padding: 10px;">
                                    <?
                                        /**
                                         * @since 3.0.0.
                                         */
                                        do_action( 'woocommerce_after_add_to_cart_quantity' );
                                    ?>
                                	</div>

                                    <button class="btn_blue product__btn-buy" onclick="" type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>">Купить</button>

                                    <?php
                                        /**
                                         * @since 2.1.0.
                                         */
                                        do_action( 'woocommerce_after_add_to_cart_button' );
                                    ?>
                                </form>
                    </div>                    
                </div>
            </div>
            <!-- end product -->


	<?php
		/**
		 * woocommerce_before_single_product_summary hook.
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		//do_action( 'woocommerce_before_single_product_summary' );
	?>

	<?// <div class="summary entry-summary"> //?>

		<?php
			/**
			 * woocommerce_single_product_summary hook.
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 * @hooked WC_Structured_Data::generate_product_data() - 60
			 */
			//do_action( 'woocommerce_single_product_summary' );
		?>

	<?// </div><!-- .summary --> //?>

	<?php
		/**
		 * woocommerce_after_single_product_summary hook.
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		//do_action( 'woocommerce_after_single_product_summary' );
	?>

			
                  



 <? //</div> ?><!-- #product-<?php the_ID(); ?> --> 

<?php do_action( 'woocommerce_after_single_product' ); ?>

<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package storefront
 */

get_header(); ?>

	<? /*
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) :

			get_template_part( 'loop' );

		else :

			get_template_part( 'content', 'none' );

		endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->
	*/ ?>

	<!-- begin wrap  -->
    <div class="wrap">

        <!-- begin container  -->
        <div class="content container">



            <!-- begin breadcrumbs  -->
            <?php
				/**
				 * woocommerce_before_main_content hook.
				 *
				 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
				 * @hooked woocommerce_breadcrumb - 20
				 */
				//do_action( 'woocommerce_before_main_content' );
			?>
			<?php //get_sidebar('true_side' ); ?>
            <!-- end breadcrumbs -->
            	<? //include "menu.php"; ?>
    <?php
		/**
		 * woocommerce_sidebar hook.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		//do_action( 'woocommerce_sidebar' );
	?>

	<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 * @hooked WC_Structured_Data::generate_website_data() - 30
		 */
		//do_action( 'woocommerce_before_main_content' );
	?>

		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

			
	    <? $images = get_field('gallery', 'option');
			if (!empty( $images )):	?>
			<!-- begin home-slider  -->
	        <div class="home-slider container js-home-slider">
	        	 <div class="home-slider container js-home-slider">
	        	 	<?	foreach ( $images as $image ) : ?>
					<div class="item">
						<img src="<?php echo $image['url']; ?>" width="1200" height="600" alt="slide"" />
						<?	//echo wp_get_attachment_image( $image['ID'], 'full' ); ?>
					</div>
						<? endforeach;	?> 
				</div>
			</div>
			<?	endif; ?>	
	        
	        <!-- end home-slider -->

			<div class="content ">
                <h1 class="title"><span>ПОПУЛЯРНЫЕ ФОТООБОИ</span></h1>
                <div class="wallpapers__category">

			<?php 
			global $product;
			$a = 0;
			echo $a;
			
			?>

		<?php endif; ?>

		<?php
			/**
			 * woocommerce_archive_description hook.
			 *
			 * @hooked woocommerce_taxonomy_archive_description - 10
			 * @hooked woocommerce_product_archive_description - 10
			 */
			//do_action( 'woocommerce_archive_description' );
		?>

		<?php if ( have_posts() ) : ?>

			<?php
				/**
				 * woocommerce_before_shop_loop hook.
				 *
				 * @hooked wc_print_notices - 10
				 * @hooked woocommerce_result_count - 20
				 * @hooked woocommerce_catalog_ordering - 30
				 */
				do_action( 'woocommerce_before_shop_loop' );
			?>

			
			<?php woocommerce_product_loop_start(); ?>                
                
				<?php woocommerce_product_subcategories(); //<img src="../images/popular/pw1.jpg" alt="">?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php
						/**
						 * woocommerce_shop_loop hook.
						 *
						 * @hooked WC_Structured_Data::generate_product_data() - 10
						 */
						do_action( 'woocommerce_shop_loop' );
					?>

					<?php wc_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; // end of the loop. ?>

			<?php woocommerce_product_loop_end(); ?>

			<?php
				/**
				 * woocommerce_after_shop_loop hook.
				 *
				 * @hooked woocommerce_pagination - 10
				 */
				//do_action( 'woocommerce_after_shop_loop' );
			?>

		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

			<?php
				/**
				 * woocommerce_no_products_found hook.
				 *
				 * @hooked wc_no_products_found - 10
				 */
				//do_action( 'woocommerce_no_products_found' );
			?>

		<?php endif; ?>

	<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		//do_action( 'woocommerce_after_main_content' );
	?>

	
				</div>
			</div>
        </div>
    <!-- end category -->

    </div>

	<? include "why.php"; ?>

</div>

<?php
//do_action( 'storefront_sidebar' );
get_footer();

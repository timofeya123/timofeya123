<?php
/**
 * Storefront engine room
 *
 * @package storefront
 */

/**
 * Assign the Storefront version to a var
 */
$theme              = wp_get_theme( 'storefront' );
$storefront_version = $theme['Version'];

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 980; /* pixels */
}

$storefront = (object) array(
	'version' => $storefront_version,

	/**
	 * Initialize all the things.
	 */
	//'main'       => require 'inc/class-storefront.php',
	//'customizer' => require 'inc/customizer/class-storefront-customizer.php',
);

require 'inc/storefront-functions.php';
require 'inc/storefront-template-hooks.php';
require 'inc/storefront-template-functions.php';

if ( class_exists( 'Jetpack' ) ) {
	//$storefront->jetpack = require 'inc/jetpack/class-storefront-jetpack.php';
}

if ( storefront_is_woocommerce_activated() ) {
	//$storefront->woocommerce = require 'inc/woocommerce/class-storefront-woocommerce.php';

	//require 'inc/woocommerce/storefront-woocommerce-template-hooks.php';
	//require 'inc/woocommerce/storefront-woocommerce-template-functions.php';
}

if ( is_admin() ) {
	//$storefront->admin = require 'inc/admin/class-storefront-admin.php';

	//require 'inc/admin/class-storefront-plugin-install.php';
}

/**
 * NUX
 * Only load if wp version is 4.7.3 or above because of this issue;
 * https://core.trac.wordpress.org/ticket/39610?cversion=1&cnum_hist=2
 */
if ( version_compare( get_bloginfo( 'version' ), '4.7.3', '>=' ) && ( is_admin() || is_customize_preview() ) ) {
	//require 'inc/nux/class-storefront-nux-admin.php';
	//require 'inc/nux/class-storefront-nux-guided-tour.php';

	if ( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '3.0.0', '>=' ) ) {
		//require 'inc/nux/class-storefront-nux-starter-content.php';
	}
}

/*
 * Note: Do not add any custom code here. Please use a custom plugin so that your customizations aren't lost during updates.
 * https://github.com/woocommerce/theme-customisations
 */

function fleet_styles() {
    wp_enqueue_style( 'admin.css', get_stylesheet_directory_uri() . '/css/admin.css' );
    wp_enqueue_style( 'vendors.min',    get_stylesheet_directory_uri() . '/css/vendors.min.css' );
    wp_enqueue_style( 'main.min',    get_stylesheet_directory_uri() . '/css/main.min.css' );
}
add_action( 'wp_enqueue_scripts', 'fleet_styles' );

function fleet_scripts() {
 	wp_register_script( 'vendors.min', get_template_directory_uri() . '/js/vendors.min.js' );
    wp_register_script( 'main.min', get_template_directory_uri() . '/js/main.min.js' );
    wp_register_script( 'select2.full.min', get_template_directory_uri() . '/js/select2.full.min.js' );
     
    wp_enqueue_script( 'vendors.min' );
    wp_enqueue_script( 'main.min' );
    wp_enqueue_script( 'select2.full.min' );
    wp_enqueue_script( 'jquery');
}
add_action( 'wp_enqueue_scripts', 'fleet_scripts' );


if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();	
}

// WooCommerce products and categories/subcategories separately in archive pages
function mynew_product_subcategories( $args = array() ) {
	$parentid = get_queried_object_id();
	$args = array(
	    'parent' => $parentid
	);
	$terms = get_terms( 'product_cat', $args );
	if ( $terms ) {   
	        foreach ( $terms as $term ) {              
	            echo '<div class="wallpapers__item action">';                        
	                woocommerce_subcategory_thumbnail( $term ); 
	                echo '<div class="wallpapers__info">';
	                	echo '<div class="wallpapers__info-item wallpapers__title">';
	                		echo $term->name;
	                	echo '</div>';
	                    echo '<div class="wallpapers__info-item">';
	                        echo '<a class="wallpapers__link" href="'.  esc_url( get_term_link( $term ) ) .'">Посмотреть</a>';
	                    echo '</div>';
				echo '</div>';
           	echo '</div>';
	                    //echo '<a href="'  '" class="' . $term->slug . '">';                                                    
	    }
	}
}
add_action( 'woocommerce_before_shop_loop', 'mynew_product_subcategories', 50 );

// WooCommerce products and categories/subcategories separately in archive pages
function mynew_woocommerce_sidebar( $args = array() ) {
	$parentid = get_queried_object_id();
			$args = array(
			    'parent' => $parentid
			);
			$terms = get_terms( 'product_cat', $args );
			if ( $terms ) {   
				 echo '<div class="sidebar sidebar-menu">';
		                echo '<h4 class="sidebar-menu__title">ФОТООБОИ</h4>';
		                echo '<ul>';

			        foreach ( $terms as $term ) {                 
			            	echo '<li><a href="'.  esc_url( get_term_link( $term ) ) .'">'.$term->name.'</a></li>';      
			            }
			            echo '</ul>';
		        echo '</div>';
			}
}
add_action( 'woocommerce_sidebar', 'mynew_woocommerce_sidebar', 10 );

function wallpapers_register_wp_sidebars() {
 
    /* В боковой колонке - первый сайдбар */
    register_sidebar(
        array(
            'id' => 'true_side', // уникальный id
            'name' => 'Боковая колонка', // название сайдбара
            'description' => 'Перетащите сюда виджеты, чтобы добавить их в сайдбар.', // описание
            'before_widget' => '<div id="%1$s" class="side widget %2$s">', // по умолчанию виджеты выводятся <li>-списком
            'after_widget' => '</div>',
            'before_title' => '<h4 class="sidebar-menu__title">', // по умолчанию заголовки виджетов в <h2>
            'after_title' => '</h4>'
        )
    );
 
    /* В подвале - второй сайдбар */
    register_sidebar(
        array(
            'id' => 'true_foot',
            'name' => 'Футер',
            'description' => 'Перетащите сюда виджеты, чтобы добавить их в футер.',
            'before_widget' => '<div id="%1$s" class="foot widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>'
        )
    );
}
 
add_action( 'widgets_init', 'wallpapers_register_wp_sidebars' );


add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {
     unset($fields['shipping']['shipping_first_name']);
     unset($fields['shipping']['shipping_company']);
     unset($fields['shipping']['shipping_address_1']);
     unset($fields['shipping']['shipping_address_2']);
     unset($fields['shipping']['shipping_city']);
     unset($fields['shipping']['shipping_postcode']);
     unset($fields['shipping']['shipping_country']);
     unset($fields['shipping']['shipping_state']);
     unset($fields['shipping']['shipping_last_name']);
     unset($fields['shipping']['shipping_phone']);
     //unset($fields['order']['order_comments']);
     

     unset($fields['billing']['billing_first_name']);
     unset($fields['billing']['billing_company']);
     unset($fields['billing']['billing_address_1']);
     unset($fields['billing']['billing_address_2']);
     unset($fields['billing']['billing_city']);
     unset($fields['billing']['billing_postcode']);
     unset($fields['billing']['billing_country']);
     unset($fields['billing']['billing_state']);
     unset($fields['billing']['billing_last_name']);
     unset($fields['billing']['billing_phone']);
     unset($fields['billing']['billing_email']);
     //echo $a = '<p>строка</p>';
      $fields['billing']['billing_first_name'] = array(
      	'required' => true,
        );
     $fields['billing']['billing_first_name'] = array(
        'type'          => 'text',
        'class'         => array('my-field-class form-row-wide'),
        'label'         => ('Имя*'),
        'placeholder'   => ('Введите Имя'),
        'required' => true,
        );
     $fields['billing']['billing_last_name'] = array(
        'type'          => 'text',
        'class'         => array('my-field-class form-row-wide'),
        'label'         => ('Фамилия*'),
        'placeholder'   => ('Введите Фамилию'),
        'required' => true,
        );
     $fields['billing']['billing_phone'] = array(
        'type'          => 'text',
        'class'         => array('my-field-class form-row-wide'),
        'label'         => ('Телефон*'),
        'placeholder'   => ('Введите телефон'),
        'required' => true,
        );
     $fields['billing']['billing_email'] = array(
        'type'          => 'text',
        'class'         => array('my-field-class form-row-wide'),
        'label'         => ('Электронная почта*'),
        'placeholder'   => ('Введите электронную почту'),
        'required' => true,
        );
      $fields['shipping']['shipping_street'] = array(
        'type'          => 'text',
        'class'         => array('my-field-class form-row-wide'),
        'label'         => ('Улица'),
        'placeholder'   => ('Введите улицу'),
        'required' => true,
        );
      $fields['shipping']['shipping_home'] = array(
        'type'          => 'text',
        'class'         => array('my-field-class form-row-wide'),
        'label'         => ('Дом'),
        'placeholder'   => ('Введите дом'),
        'required' => true,
        );
      $fields['shipping']['shipping_home_number'] = array(
        'type'          => 'text',
        'class'         => array('my-field-class form-row-wide'),
        'label'         => ('Квартира'),
        'placeholder'   => ('Введите номер квартиры'),
        'required' => true,
        );
     /*
     $fields['billing']['patronymic_name'] = array(
        'type'          => 'text',
        'class'         => array('my-field-class form-row-wide'),
        'label'         => ('Patronymic Name'),
        'placeholder'   => ('Enter Patronymic'),
        );
        */
     return $fields;
}

/**
 * Add the field to the checkout
 */
//add_action( 'woocommerce_after_order_notes', 'my_custom_checkout_field' );

function my_custom_checkout_field( $checkout ) {
    woocommerce_form_field( 'patronymic_name', array(
        'type'          => 'text',
        'class'         => array('my-field-class form-row-wide'),
        'label'         => ('Patronymic Name'),
        'placeholder'   => ('Enter Patronymic'),
        ), $checkout->get_value( 'patronymic_name' ));

    echo '</div>';

}

/**
 * Update the order meta with field value
 */
add_action( 'woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta' );

function my_custom_checkout_field_update_order_meta( $order_id ) {
    if ( ! empty( $_POST['shipping_street'] ) ) {
        update_post_meta( $order_id, 'Street', sanitize_text_field( $_POST['shipping_street'] ) );
    }
    if ( ! empty( $_POST['shipping_home'] ) ) {
        update_post_meta( $order_id, 'Home', sanitize_text_field( $_POST['shipping_home'] ) );
    }
    if ( ! empty( $_POST['shipping_home_number'] ) ) {
        update_post_meta( $order_id, 'Home Number', sanitize_text_field( $_POST['shipping_home_number'] ) );
    }
}

/**
 * Display field value on the order edit page
 */
/*
add_action( 'woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );

function my_custom_checkout_field_display_admin_order_meta($order){
    echo '<p><strong>'.('Улица').':</strong> ' . get_post_meta( $order->id, 'Street', true ) . '</p>';
    echo '<p><strong>'.('Дом').':</strong> ' . get_post_meta( $order->id, 'Home', true ) . '</p>';
    echo '<p><strong>'.__('Номер квартиры').':</strong> ' . get_post_meta( $order->id, 'Home Number', true ) . '</p>';
}

add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );

function woocommerce_header_add_to_cart_fragment( $fragments ) {

ob_start(); ?>

  <div class="cart-price"><p><span class="first-name">Товаров:</span> <span id="cart_total_amount"><?php echo sprintf (_n( '%d', '%d', WC()->cart->cart_contents_count ), WC()->cart->cart_contents_count ); ?></span></p><p><span class="first-name">На сумму:</span> <span id="cart_total" class="pink-price"><?php echo WC()->cart->get_cart_total(); ?></span></p></div> 

  <?php $fragments['div.cart-price'] = ob_get_clean(); // селектор блока обертки

  return $fragments;

}
/**
 * Auto update cart after quantity change 
 * 
 * @return string 
 **/
 /* 
add_action( 'wp_footer', 'cart_update_qty_script' ); 
function cart_update_qty_script() { 
	if (is_cart()) : 
		?> 
		<script> 
			jQuery('div.woocommerce').on('change', '.quantity .button', function(){
			 jQuery("[name='update_cart']").trigger("click"); 
			});
		</script> 
	<?php endif; 
}
*/
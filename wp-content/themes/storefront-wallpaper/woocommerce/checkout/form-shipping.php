<?php
/**
 * Checkout shipping information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-shipping.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.9
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<?php if ( true === WC()->cart->needs_shipping_address() ) : ?>
<div class="cart__footer-col cart__shipping">
    <div class="cart__title cart__title_shipping">ДОСТАВКА</div>
    <div class="cart__shipping-row">
        <div class="cart__shipping-col">Доставка</div>
        <div class="cart__shipping-col _right">$10</div>
    </div>
    <select class="custom-select js-custom-select" data-placeholder="Область" name="" id="">
        <option></option>
        <option>Омск</option>
        <option>Омск</option>
        <option>Омск</option>
    </select>
    <select class="custom-select js-custom-select" data-placeholder="Город" name="" id="">
        <option></option>
        <option>Омск</option>
        <option>Омск</option>
        <option>Омск</option>
    </select>
    <div class="cart__shipping-row _shipping">
        <div class="cart__shipping-col">
            <input id="shipping" type="radio" name="shipping">
            <label for="shipping">Доставка на дом</label>
        </div>
        <div class="cart__shipping-col _right">$10</div>
    </div>
	<div class="woocommerce-shipping-fields">
		

			<?/*
		<h3 id="ship-to-different-address">
			<label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
				<input id="ship-to-different-address-checkbox" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" <?php checked( apply_filters( 'woocommerce_ship_to_different_address_checked', 'shipping' === get_option( 'woocommerce_ship_to_destination' ) ? 1 : 0 ), 1 ); ?> type="checkbox" name="ship_to_different_address" value="1" /> <span><?php _e( 'Ship to a different address?', 'woocommerce' ); ?></span>
			</label>
		</h3>
		*/?>

		<div class="shipping_address">

			<?php do_action( 'woocommerce_before_checkout_shipping_form', $checkout ); ?>

			<div class="woocommerce-shipping-fields__field-wrapper">
			<?php
				$fields = $checkout->get_checkout_fields( 'shipping' );

				foreach ( $fields as $key => $field ) {
					if ( isset( $field['country_field'], $fields[ $field['country_field'] ] ) ) {
						$field['country'] = $checkout->get_value( $field['country_field'] );
							}
							woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
						}
					?>
				</div>

			<?php do_action( 'woocommerce_after_checkout_shipping_form', $checkout ); ?>

		</div>

		
	</div>
	<p>Срок доставки 5-7 дней <br> Курьер позвонит Вам на указанный номер телефона</p>
    <div class="cart__shipping-ro">
        <div class="cart__shipping-col">
            <input id="self-delivery" type="radio" name="shipping">
            <label for="self-delivery">Самовывоз (недоступен)</label>
        </div>
        <div class="cart__shipping-col"></div>
    </div>
</div>
<div class="cart__footer-col cart__information">
        <div class="cart__title">Информация для доставки</div>
	<?php do_action( 'woocommerce_before_order_notes', $checkout ); ?>

	<?php if ( apply_filters( 'woocommerce_enable_order_notes_field', 'yes' === get_option( 'woocommerce_enable_order_comments', 'yes' ) ) ) : ?>

		<?php /*if ( ! WC()->cart->needs_shipping() || wc_ship_to_billing_address_only() ) : ?>

			<h3><?php _e( 'Additional information', 'woocommerce' ); ?></h3>

		<?php endif; */?>

		<div class="woocommerce-additional-fields__field-wrapper">
			<?php foreach ( $checkout->get_checkout_fields( 'order' ) as $key => $field ) : ?>
				<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>
			<?php endforeach; ?>
		</div>

	<?php endif; ?>

	<?php do_action( 'woocommerce_after_order_notes', $checkout ); ?>

	<div class="form-row place-order">
		<? global $order_button_text; ?>
		<noscript>
			<?php _e( 'Since your browser does not support JavaScript, or it is disabled, please ensure you click the <em>Update Totals</em> button before placing your order. You may be charged more than the amount stated above if you fail to do so.', 'woocommerce' ); ?>
			<br/><input type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="<?php esc_attr_e( 'Update totals', 'woocommerce' ); ?>" />
		</noscript>

		<?php wc_get_template( 'checkout/terms.php' ); ?>

		<?php do_action( 'woocommerce_review_order_before_submit' ); ?>

		<?php echo apply_filters( 'woocommerce_order_button_html', '<input type="submit" class="btn btn_blue btn-create-payment" name="woocommerce_checkout_place_order" id="place_order" value="Создать и оплатить" data-value="Создать и оплатить" />' ); ?>

		<?php do_action( 'woocommerce_review_order_after_submit' ); ?>
	</div>
	<div style="display: none;">
	<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>
	<?php do_action( 'woocommerce_checkout_order_review' ); ?>
	</div>
</div>
<?php endif; ?>

<?
/*
	if (isset($_GET["Search"])) {
		header("Location: http://wordpress/boats/");
		exit;
	}
	*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<title>Demo Site</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
	</head>
	<body>
	<header>
		<div class="header-layout">
			<div class="color-line" style="background-color: #30b58c">
				<div class="container">
					<div class="row">
						<div class="col-md-3 col-sm-3">
							<div class="logo">
								<a href="#">
									<? $image = get_field('logotype', 'option');
										if( !empty($image) ): ?>

										<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

									<?php endif; ?>
								</a>
							</div>
						</div>
						
						<div id="phoneholder" class="col-md-3 col-sm-3" style="padding-right:0px">
							<ul class="header-items" style="padding-top: 12px; margin-left:28px;">
								<li class="phone-holder" style="width:100%">
									<p>
									<? $value = get_field( "text", 'option' );

											if( $value ) {
											    
											    echo $value;

											} else {

											    echo 'empty';
											    
											} ?>
									</p>
								</li>
								<li style="margin:0px; width:100%">
									<a style="text-align: center;" href="tel:850-837-1995">
										<? $value = get_field( "phone_1", 'option' );

												if( $value ) {
												    
												    echo $value;

												} else {

												    echo 'empty';
												    
												} ?>
									</a>
								</li>
								<li style="margin:0px;width:100%">
									<a style="text-align: center;" href="tel:8778371981">
										<? $value = get_field( "phone_2", 'option' );

												if( $value ) {
												    
												    echo $value;

												} else {

												    echo 'empty';
												    
												} ?>
									</a>
								</li>
							</ul>
						</div>
						
						<? if( have_rows( 'soc', 'option' ) ) {
								while ( have_rows( 'soc', 'option') ) 
								{ the_row(); ?>
								<div class="col-md-4 col-sm-3">
									<ul class="header-items" style="color: #ffffff">							
										<li class="contact-holder"  style="color: rgb(255, 255, 255); cursor: pointer;" class="popmake-62 pum-trigger">
											<?	wp_nav_menu( array ( 
												'container' => false, 
												'theme_location' => 'Contact', 
												'items_wrap' => '<a id="%1$s" class="popmake-62 pum-trigger">%3$s</a>') );	?>
										</li>
										<li class="social-holder">
											<? the_sub_field('follow');	?>
											<a href="#" target="_blank">
												<? $image = get_sub_field('image_1');
													if( !empty($image) ) { ?>

												<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

												<?php } ?>
											</a>
										</li>
									</ul>
								</div>
								<? }
								  }
								  else { echo 'empty'; } ?>
									
						<? $image = get_field('image_1', 'option');
								if( !empty($image) ) : ?>	
						<div class="col-md-2 col-sm-3" style="text-align:center;">
							
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" style="height: 112px; padding:5px;" />							
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>	
<?php
session_start();
get_header();
?>		
	
<? get_search_form(); ?>

	<div id="main-container">
		<div class="container">	
			<?php
				$args = array(
					'post_type' => 'post',
					'publish' => true,
					'paged' => get_query_var('paged'),
					'posts_per_page' => 3,
					);
					
				$tt = new WP_Query($args);

				if ( $tt->have_posts() ) :
			?>		
			<section class="row discount">
				<div class="col-md-12 ">
					<h2>Discounts And Offers</h2>
				</div>	
					<?php
						while ( $tt->have_posts() ) :
							$tt->the_post(); 
					?>
				<div class="col-md-4 col-sm-4">
					<div class="discount-card">
						<div class="figure">
							<? the_post_thumbnail(); ?>	
						</div>
						<div class="mark">
							<? the_title();	?>
						</div>
						<p>
							<? the_excerpt(); ?>	
						</p>
					</div>
				</div>
				<? endwhile; ?>
			</section>
			<? endif; ?>

			<?	$argss = array(
							'post_type' => 'boats',
							'publish' => true,
							'paged' => get_query_var('paged'),
							'posts_per_page' => 6,
							);
					
						$tt = new WP_Query($argss);

						if ( $tt->have_posts() ) :
			?>
			<section class="row recommence">
				<div class="col-md-12">
					<h1 style="text-align: center;">Lorem Ipsum is simply dummy text of the the printing</h1>
					<p style="text-align: center;">
						Lorem Ipsum is simply dummy text of the the printing
					</p>
				</div>	
					<?php
						while ( $tt->have_posts() ) :
							$tt->the_post(); 
					?>
				<div class="col-md-4 col-sm-6">						
					<div class="recommence-card">
						<div class="recommence-card__content">
							<div class="recommence-card__mark">recommended</div>						
								<? the_post_thumbnail( $size= 'h-thumb'); ?>
							<div class="recommence-card__description">
								<div class="title">
									<? the_title();	?>
								</div>
								<p class="price"> from <span>
									<?php	
										$value = get_field( "price_range" );
										if( $value ) {
										    echo "$".$value;
										} else {
										    echo 'empty';
										} 
									?>
								</span>per hour </p>
								<div class="options">
									<i class="icon-yacht"></i> <span>
										<?php	
										$value = get_field( "vessel_size" );
										if( $value ) {
										    echo $value;
										} else {
										    echo 'empty';
										} 
										?>
									 feet</span>
								</div>
								<div class="options">
									<i class="icon-person"></i> <span>
										<?php	
											$value = get_field( "vessel_occupancy" );
											if( $value ) {
											    echo $value;
											} else {
											    echo 'empty';
											} 
										?>
									 people</span>
								</div>
							</div>
						</div>				
					</div>
				</div>
				<?php endwhile; ?>
			</section>
		<?php endif; ?>
		</div>
	</div>

<?php get_footer(); ?>
<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?
				$args = array(
					'post_type' => 'discount_&_offers',
					'publish' => true,
					'paged' => get_query_var('paged'),
					);
					
				$tt = new WP_Query($args);

				if ( $tt->have_posts() ) {
			?>		
			<section class="row discount">
				<?php get_search_form();?>
				<div class="col-md-12 ">
					<h2>Discounts And Offers</h2>
				</div>	
					<?
						while ( $tt->have_posts() ) {
							$tt->the_post(); 
					?>
				<div class="col-md-4 col-sm-4">
					<div class="discount-card">
						<div class="figure">
							<? the_post_thumbnail(); ?>	
						</div>
						<div class="mark">
							<? the_title();	?>
						</div>
						<p>
							<? the_excerpt(); ?>	
						</p>
					</div>
				</div>
				<? } ?>
			</section>
			<? } ?>

		</main><!-- #main -->
	</div><!-- #primary -->
	<?php get_sidebar(); ?>
</div><!-- .wrap -->

<?php get_footer();

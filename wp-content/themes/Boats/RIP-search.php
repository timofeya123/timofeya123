<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Boats
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div>
		<h3><? get_search_form();?></h3>
	</div>


			<?php
				if ( have_posts() ) {
			?>		
			<section class="row discount">
				<div class="col-md-12 ">
					<h2>Discounts And Offers</h2>
				</div>	
					<?
						while ( have_posts() ) {
							the_post(); 
					?>
				<div class="col-md-4 col-sm-4">
					<div class="discount-card">
						<div class="figure">
							<? the_post_thumbnail(); ?>	
						</div>
						<div class="mark">
							<? the_title();	?>
						</div>
						<p>
							<? the_excerpt(); ?>	
						</p>
					</div>
				</div>
				<? } ?>
			</section>
			<? } ?>
			

<?php get_footer();

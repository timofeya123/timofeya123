<?php get_header(); ?>

<?php 	if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();	?>
<div id="main-container">
		<div class="container">
			<div class="row">
				<div style="font-size:16px" class="breadcrumbs-top">
					<a href="http://wordpress/">Home</a>
					&nbsp;≫&nbsp;
					<a href="javascript:window.history.back();">
					Search	
					</a>&nbsp;≫&nbsp;
						<?	$aw = the_title();
							if ($menu_items = wp_get_nav_menu_items('primary')){
								$menu_list ='';
								foreach ( (array) $menu_items as $key => $menu_items) {
									$title = $menu_items->title;
									$url= $menu_items->url;
									if ($aw == $title){
									$menu_list .= '<a href="'.$url.'">'.$title.'</a>';
									}
								}
								echo $menu_list;	
							}	?>
				</div>
				<div class="single-product__header">
					<div class="row" id="single-header-booknow">
						<div class="col-md-7">
							<h1>
								<?	the_title(); ?>
							</h1>
						</div>				
					</div>
				</div>
				
				<div class="row" id="main-stuff">
					<div class="col-md-7">
						<div class="js-single-product-slider single-product__slider owl-theme owl-carousel" style="opacity: 1; display: block;">
							<div class="owl-wrapper-outer">
								<div class="owl-wrapper">
									<? $images = get_field( 'gallery' );
										$size = 'full';

										if ( $images ){
										foreach ( $images as $image ) { ?>
									<div class="owl-item" style="width: 606px;">
										<div class="item">
											<?	echo wp_get_attachment_image( $image['ID'], $size ); ?>
										</div>
									</div>
									<?	}
									}	?>										
								</div>
							</div>
							<div class="owl-controls clickable">
								<div class="owl-pagination">
									<? $images = get_field( 'gallery' );
										$size = 'full';

										if ( $images ){
										foreach ( $images as $image ) { ?>
									<div class="owl-page"><span class=""></span></div>
									<?	
										}
									}	
									?>
								</div>
								<div class="owl-buttons">
									<div class="owl-prev">prev</div>
									<div class="owl-next">next</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-5">
						<h2>
							<strong class="price-range">Price Range:</strong>
							<?
									$value = get_field( "price_range" );

											if( $value ) {
											    
											    echo "$".$value;

											} else {

											    echo 'empty';
											    
											}
									?> per hour
						</h2>

						<div class="single-product__content-style">
							<span class="single-product__content-style-label">Vessel Size:</span>
							<span class="single-product__content-style-val">
							<?
									$value = get_field( "vessel_size" );

											if( $value ) {
											    
											    echo $value;

											} else {

											    echo 'empty';
											    
											}
									?>
							 feet</span>
						</div>

						<div class="single-product__content-style">
							<span class="single-product__content-style-label">Vessel Speed:</span>
							<span class="single-product__content-style-val">
								<?
									$value = get_field( "vessel_speed" );

											if( $value ) {
											    
											    echo $value;

											} else {

											    echo 'empty';
											    
											}
									?>
							 knots +</span>
						</div>

						<div class="single-product__content-style">
							<span class="single-product__content-style-label">Vessel Occupancy:</span>
							<span class="single-product__content-style-val">
								<?
									$value = get_field( "vessel_occupancy" );

											if( $value ) {
											    
											    echo $value;

											} else {

											    echo 'empty';
											    
											}
									?>
							 people</span>
						</div>

						<div class="single-product__content-style _divided">
							<span class="single-product__content-label">Boat Type:</span>
						</div>
										
							<div class="single-product__checked-wrap-wide">
								
							<?
									$value = get_field( "checkbox" );

											if( $value ) {
											    
											    foreach ($value as $val) {
											    	?>
											    	<span class="single-product__checked">
											    	<?php
											    	echo $val;
											    	?>
											    	</span>
											    	<?php
											    }

											} else {

											    echo 'empty';
											    
											}
									?>	
						</div>

						
						<div class="single-product__content-style _divided">
							<span class="single-product__content-label">Vessel Features:</span>
						</div>		
							<div class="single-product__checked-wrap-wide">
							<?
							the_terms($post->ID, 'Boat_features', '<span class="single-product__checked">', '</span><span class="single-product__checked">', '</span>');	
							?>
						</div>
					</div>
				</div>
				
				
						<?
							if(have_rows('cont')){
								while (have_rows('cont')) 
									{ 
									?>
									<div class="row single-product__about">
										<div class="col-md-3">
											<h3>
										<?
										the_row();
										the_sub_field('name');
										?>
											</h3>
										</div>
										<div class="col-md-9">
											<p>	
										<?
										the_sub_field('text-count');
										?>
											</p>
										</div>
									</div>
									<?
								}
							}								
						?>
				
				<div class="row single-product__about">
					<div class="col-md-3">
						<h3>Excursion Types</h3>
					</div>
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-4 col-sm-4">
								<?
								the_terms($post->ID, 'Excursion_Types', '<span class="single-product__checked">', '</span><span class="single-product__checked">', '</span>');
								?>
							</div>
						<div class="col-md-4 col-sm-4"><span class="single-product__checked">Snorkeling</span><span class="single-product__checked">Wedding</span>					</div>
					</div>
				</div>
			
						<div class="row single-product__about">
					<div class="col-md-3">
						<h3>Excursion Lengths</h3>
					</div>
					<div class="col-md-9">
						<div class="row">
								<div class="col-md-4 col-sm-4">
									<?
									the_terms($post->ID, 'Excursion_Lengths', '<span class="single-product__checked">', '</span><span class="single-product__checked">', '</span>');
									?>
								</div>					
							</div>
					</div>
				</div>
			
						<div class="row single-product__about">
					<div class="col-md-3">
						<h3>Types of Fishing</h3>
					</div>
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-4 col-sm-4">
								<?
								the_terms($post->ID, 'Types of Fishing', '<span class="single-product__checked">', '</span><span class="single-product__checked">', '</span>');
								?>
							</div>					
						</div>
					</div>
				</div>
			
						<div class="row single-product__about">
					<div class="col-md-3">
						<h3>Species Fished (subject to Season)</h3>
					</div>
					<div class="col-md-9">
						<div class="row">
						<div class="col-md-4 col-sm-4">
							<?
							the_terms($post->ID, 'Species_Fished', '<span class="single-product__checked">', '</span><span class="single-product__checked">', '</span>');
							?>					
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
		</div>
	</div>
				
				
			</div>
		</div>
	</div>
		<?
		//
		// Post Content here
		//
	} // end while
	//wp_reset_postdate();
} // end if
?>

<?php get_footer(); ?>
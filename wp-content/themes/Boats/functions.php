<?php

function boats_setup(){
	load_theme_textdomain('boats');
	add_theme_support('title-tag');
	add_theme_support('custom-logo', array(
		'flex-height' => true, 
	));
	add_theme_support('post-thumbnails');

}
add_action( 'after_setup_theme', 'boats_setup' );


function boats_styles() {
	wp_enqueue_style( 'style-css', get_stylesheet_uri());
	wp_enqueue_style( 'main', get_template_directory_uri().'/css/main.min.css');
	wp_enqueue_style( 'vendors', get_template_directory_uri().'/css/vendors.min.css');

	wp_enqueue_script( 'jquery');
	wp_enqueue_script( 'vendors', get_template_directory_uri().'/js/vendors.min.js');
	wp_enqueue_script( 'ion-rangeSlider', get_template_directory_uri().'/js/ion.rangeSlider.min.js');
	wp_enqueue_script( 'jquery-mCustomScrollbar', get_template_directory_uri().'/js/jquery.mCustomScrollbar.min.js');
	wp_enqueue_script( 'jquery-mousewheel', get_template_directory_uri().'/js/jquery.mousewheel.min.js');
	wp_enqueue_script( 'typeahead', get_template_directory_uri().'/js/typeahead.js');
	wp_enqueue_script( 'main', get_template_directory_uri().'/js/main.min.js');
}
add_action( 'wp_enqueue_scripts', 'boats_styles' );

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}

function register_post_types(){
	register_post_type('Boats', array(
		'label'  => null,
		'labels' => array(
			'name'               => 'Boats', // основное название для типа записи
			'singular_name'      => 'Boats', // название для одной записи этого типа
			'add_new'            => 'Добавить Boats', // для добавления новой записи
			'add_new_item'       => 'Добавление Boats', // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => 'Редактирование Boats', // для редактирования типа записи
			'new_item'           => 'Новое Boats', // текст новой записи
			'view_item'          => 'Смотреть Boats', // для просмотра записи этого типа.
			'search_items'       => 'Искать Boats', // для поиска по этим типам записи
			'not_found'          => 'Не найдено', // если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
			'parent_item_colon'  => '', // для родителей (у древовидных типов)
			'menu_name'          => 'Boats', // название меню
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => null,
		'exclude_from_search' => null,
		'show_ui'             => null,
		'show_in_menu'        => null, // показывать ли в меню адмнки
		'show_in_admin_bar'   => null, // по умолчанию значение show_in_menu
		'show_in_nav_menus'   => null,
		'show_in_rest'        => null, // добавить в REST API. C WP 4.7
		'rest_base'           => null, // $post_type. C WP 4.7
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-post-status', 
		//'capability_type'   => 'post',
		//'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
		//'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
		'hierarchical'        => true,
		'supports'            => array('title','editor','thumbnail'), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'          => array(),
		//'has_archive'         => false,
		'rewrite'             => true,
		'has_archive'		  => true,
		'query_var'           => true,
	) );
}
add_action('init', 'register_post_types');

function register_post_types2(){
	register_post_type('Discount_&_offers', array(
		'label'  => null,
		'labels' => array(
			'name'               => 'Discount_&_offers', // основное название для типа записи
			'singular_name'      => 'Discount_&_offers', // название для одной записи этого типа
			'add_new'            => 'Добавить Discount_&_offers', // для добавления новой записи
			'add_new_item'       => 'Добавление Discount_&_offers', // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => 'Редактирование Discount_&_offers', // для редактирования типа записи
			'new_item'           => 'Новое Discount_&_offers', // текст новой записи
			'view_item'          => 'Смотреть Discount_&_offers', // для просмотра записи этого типа.
			'search_items'       => 'Искать Discount_&_offers', // для поиска по этим типам записи
			'not_found'          => 'Не найдено', // если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
			'parent_item_colon'  => '', // для родителей (у древовидных типов)
			'menu_name'          => 'Discount_&_offers', // название меню
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => null,
		'exclude_from_search' => null,
		'show_ui'             => null,
		'show_in_menu'        => null, // показывать ли в меню адмнки
		'show_in_admin_bar'   => null, // по умолчанию значение show_in_menu
		'show_in_nav_menus'   => null,
		'show_in_rest'        => null, // добавить в REST API. C WP 4.7
		'rest_base'           => null, // $post_type. C WP 4.7
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-cart', 
		//'capability_type'   => 'post',
		//'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
		//'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
		'hierarchical'        => true,
		'supports'            => array('title','editor','thumbnail'), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'          => array(),
		//'has_archive'         => false,
		'rewrite'             => true,
		'has_archive'		  => true,
		'query_var'           => true,
	) );
}
add_action('init', 'register_post_types2');

add_image_size('h-thumb', 430, 430, false);

function my_post_image_html($html, $post_id, $post_image_id){
	$html = '<a href"'.get_permalink($post_id).'" title="'.esc_attr(get_post_field('post_title', $post_id)).'">'.$html.'</a>';
	return $html;
}
add_filter('post_thumbnail_html','my_post_image_html',10,3);


function create_taxonomy(){
	// список параметров: http://wp-kama.ru/function/get_taxonomy_labels
	register_taxonomy('Excursion_Types', array('boats'), array(
		'label'                 => '', // определяется параметром $labels->name
		'labels'                => array(
			'name'              => 'Excursion Types',
			'singular_name'     => 'Excursion Types',
			'search_items'      => 'Search Excursion Types',
			'all_items'         => 'All Excursion Types',
			'view_item '        => 'View Excursion Types',
			'parent_item'       => 'Parent Excursion Types',
			'parent_item_colon' => 'Parent Excursion Types:',
			'edit_item'         => 'Edit Excursion Types',
			'update_item'       => 'Update Excursion Types',
			'add_new_item'      => 'Add New Excursion Types',
			'new_item_name'     => 'New Excursion Types Name',
			'menu_name'         => 'Excursion Types',
		),
		'description'           => '', // описание таксономии
		'public'                => true,
		'publicly_queryable'    => null, // равен аргументу public
		'show_in_nav_menus'     => true, // равен аргументу public
		'show_ui'               => true, // равен аргументу public
		'show_tagcloud'         => true, // равен аргументу show_ui
		'show_in_rest'          => null, // добавить в REST API
		'rest_base'             => null, // $taxonomy
		'hierarchical'          => true,
		'update_count_callback' => '',
		'rewrite'               => true,
		//'query_var'             => $taxonomy, // название параметра запроса
		'capabilities'          => array(),
		'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
		'show_admin_column'     => true, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
		'_builtin'              => false,
		'show_in_quick_edit'    => null, // по умолчанию значение show_ui
	) );

	register_taxonomy('Excursion_Lengths', array('boats'), array(
		'label'                 => '', // определяется параметром $labels->name
		'labels'                => array(
			'name'              => 'Excursion Lengths',
			'singular_name'     => 'Excursion Lengths',
			'search_items'      => 'Search Excursion Lengths',
			'all_items'         => 'All Excursion Lengths',
			'view_item '        => 'View Excursion Lengths',
			'parent_item'       => 'Parent Excursion Lengths',
			'parent_item_colon' => 'Parent Excursion Lengths:',
			'edit_item'         => 'Edit Excursion Lengths',
			'update_item'       => 'Update Excursion Lengths',
			'add_new_item'      => 'Add New Excursion Lengths',
			'new_item_name'     => 'New Excursion Lengths Name',
			'menu_name'         => 'Excursion Lengths',
		),
		'description'           => '', // описание таксономии
		'public'                => true,
		'publicly_queryable'    => null, // равен аргументу public
		'show_in_nav_menus'     => true, // равен аргументу public
		'show_ui'               => true, // равен аргументу public
		'show_tagcloud'         => true, // равен аргументу show_ui
		'show_in_rest'          => null, // добавить в REST API
		'rest_base'             => null, // $taxonomy
		'hierarchical'          => true,
		'update_count_callback' => '',
		'rewrite'               => true,
		//'query_var'             => $taxonomy, // название параметра запроса
		'capabilities'          => array(),
		'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
		'show_admin_column'     => true, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
		'_builtin'              => false,
		'show_in_quick_edit'    => null, // по умолчанию значение show_ui
	) );

	register_taxonomy('Types of Fishing', array('boats'), array(
		'label'                 => '', // определяется параметром $labels->name
		'labels'                => array(
			'name'              => 'Types of Fishing',
			'singular_name'     => 'Types of Fishing',
			'search_items'      => 'Search Types of Fishing',
			'all_items'         => 'All Types of Fishing',
			'view_item '        => 'View Types of Fishing',
			'parent_item'       => 'Parent Types of Fishing',
			'parent_item_colon' => 'Parent Types of Fishing:',
			'edit_item'         => 'Edit Types of Fishing',
			'update_item'       => 'Update Types of Fishing',
			'add_new_item'      => 'Add New Types of Fishing',
			'new_item_name'     => 'New Types of Fishing Name',
			'menu_name'         => 'Types of Fishing',
		),
		'description'           => '', // описание таксономии
		'public'                => true,
		'publicly_queryable'    => null, // равен аргументу public
		'show_in_nav_menus'     => true, // равен аргументу public
		'show_ui'               => true, // равен аргументу public
		'show_tagcloud'         => true, // равен аргументу show_ui
		'show_in_rest'          => null, // добавить в REST API
		'rest_base'             => null, // $taxonomy
		'hierarchical'          => true,
		'update_count_callback' => '',
		'rewrite'               => true,
		//'query_var'             => $taxonomy, // название параметра запроса
		'capabilities'          => array(),
		'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
		'show_admin_column'     => true, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
		'_builtin'              => false,
		'show_in_quick_edit'    => null, // по умолчанию значение show_ui
	) );

	register_taxonomy('Species_Fished', array('boats'), array(
		'label'                 => '', // определяется параметром $labels->name
		'labels'                => array(
			'name'              => 'Species Fished (subject to Season)',
			'singular_name'     => 'Species Fished (subject to Season)',
			'search_items'      => 'Search Species Fished (subject to Season)',
			'all_items'         => 'All Species Fished (subject to Season)',
			'view_item '        => 'View Species Fished (subject to Season)',
			'parent_item'       => 'Parent Species Fished (subject to Season)',
			'parent_item_colon' => 'Parent Species Fished (subject to Season):',
			'edit_item'         => 'Edit Species Fished (subject to Season)',
			'update_item'       => 'Update Species Fished (subject to Season)',
			'add_new_item'      => 'Add New Species Fished (subject to Season)',
			'new_item_name'     => 'New Species Fished (subject to Season) Name',
			'menu_name'         => 'Species Fished (subject to Season)',
		),
		'description'           => '', // описание таксономии
		'public'                => true,
		'publicly_queryable'    => null, // равен аргументу public
		'show_in_nav_menus'     => true, // равен аргументу public
		'show_ui'               => true, // равен аргументу public
		'show_tagcloud'         => true, // равен аргументу show_ui
		'show_in_rest'          => null, // добавить в REST API
		'rest_base'             => null, // $taxonomy
		'hierarchical'          => true,
		'update_count_callback' => '',
		'rewrite'               => true,
		//'query_var'             => $taxonomy, // название параметра запроса
		'capabilities'          => array(),
		'meta_box_cb'           => 'post_categories_meta_box', // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
		'show_admin_column'     => true, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
		'_builtin'              => false,
		'show_in_quick_edit'    => null, // по умолчанию значение show_ui
	) );

	// список параметров: http://wp-kama.ru/function/get_taxonomy_labels
	register_taxonomy('Boat_type', array('boats'), array(
		'label'                 => '', // определяется параметром $labels->name
		'labels'                => array(
			'name'              => 'Boat type',
			'singular_name'     => 'Boat type',
			'search_items'      => 'Search Boat type',
			'all_items'         => 'All Boat type',
			'view_item '        => 'View Boat type',
			'parent_item'       => 'Parent Boat type',
			'parent_item_colon' => 'Parent Boat type:',
			'edit_item'         => 'Edit Boat type',
			'update_item'       => 'Update Boat type',
			'add_new_item'      => 'Add New Boat type',
			'new_item_name'     => 'New Boat type Name',
			'menu_name'         => 'Boat type',
		),
		'description'           => '', // описание таксономии
		'public'                => true,
		'publicly_queryable'    => null, // равен аргументу public
		'show_in_nav_menus'     => true, // равен аргументу public
		'show_ui'               => true, // равен аргументу public
		'show_tagcloud'         => true, // равен аргументу show_ui
		'show_in_rest'          => null, // добавить в REST API
		'rest_base'             => null, // $taxonomy
		'hierarchical'          => true,
		'update_count_callback' => '',
		'rewrite'               => true,
		//'query_var'             => $taxonomy, // название параметра запроса
		'capabilities'          => array(),
		'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
		'show_admin_column'     => true, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
		'_builtin'              => false,
		'show_in_quick_edit'    => null, // по умолчанию значение show_ui
	) );

	register_taxonomy('Boat_features', array('boats'), array(
		'label'                 => '', // определяется параметром $labels->name
		'labels'                => array(
			'name'              => 'Boat features',
			'singular_name'     => 'Boat features',
			'search_items'      => 'Search Boat features',
			'all_items'         => 'All Boat features',
			'view_item '        => 'View Boat features',
			'parent_item'       => 'Parent Boat features',
			'parent_item_colon' => 'Parent Boat features:',
			'edit_item'         => 'Edit Boat features',
			'update_item'       => 'Update Boat features',
			'add_new_item'      => 'Add New Boat features',
			'new_item_name'     => 'New Boat features Name',
			'menu_name'         => 'Boat features',
		),
		'description'           => '', // описание таксономии
		'public'                => true,
		'publicly_queryable'    => null, // равен аргументу public
		'show_in_nav_menus'     => true, // равен аргументу public
		'show_ui'               => true, // равен аргументу public
		'show_tagcloud'         => true, // равен аргументу show_ui
		'show_in_rest'          => null, // добавить в REST API
		'rest_base'             => null, // $taxonomy
		'hierarchical'          => true,
		'update_count_callback' => '',
		'rewrite'               => true,
		//'query_var'             => $taxonomy, // название параметра запроса
		'capabilities'          => array(),
		'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
		'show_admin_column'     => true, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
		'_builtin'              => false,
		'show_in_quick_edit'    => null, // по умолчанию значение show_ui
	) );

	register_taxonomy('Boat_label', array('boats'), array(
		'label'                 => '', // определяется параметром $labels->name
		'labels'                => array(
			'name'              => 'Boat label',
			'singular_name'     => 'Boat label',
			'search_items'      => 'Search Boat label',
			'all_items'         => 'All Boat label',
			'view_item '        => 'View Boat label',
			'parent_item'       => 'Parent Boat label',
			'parent_item_colon' => 'Parent Boat label:',
			'edit_item'         => 'Edit Boat label',
			'update_item'       => 'Update Boat label',
			'add_new_item'      => 'Add New Boat label',
			'new_item_name'     => 'New Boat label Name',
			'menu_name'         => 'Boat label',
		),
		'description'           => '', // описание таксономии
		'public'                => true,
		'publicly_queryable'    => null, // равен аргументу public
		'show_in_nav_menus'     => true, // равен аргументу public
		'show_ui'               => true, // равен аргументу public
		'show_tagcloud'         => true, // равен аргументу show_ui
		'show_in_rest'          => null, // добавить в REST API
		'rest_base'             => null, // $taxonomy
		'hierarchical'          => false,
		'update_count_callback' => '',
		'rewrite'               => true,
		//'query_var'             => $taxonomy, // название параметра запроса
		'capabilities'          => array(),
		'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
		'show_admin_column'     => true, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
		'_builtin'              => false,
		'show_in_quick_edit'    => null, // по умолчанию значение show_ui
	) );
}
// хук для регистрации
add_action('init', 'create_taxonomy');

function boats_registr_nav_menu(){
	register_nav_menu('primary', 'Primary menu');
	register_nav_menu('Contact', 'Contact us');
}
add_action('after_setup_theme', 'boats_registr_nav_menu');

$args = array(
			'show_all'     => true, // показаны все страницы участвующие в пагинации
			'end_size'     => 1,     // количество страниц на концах
			'mid_size'     => 1,     // количество страниц вокруг текущей
			'prev_next'    => true,  // выводить ли боковые ссылки "предыдущая/следующая страница".
			'prev_text'    => __('« Previous'),
			'next_text'    => __('Next »'),
			'add_args'     => false, // Массив аргументов (переменных запроса), которые нужно добавить к ссылкам.
			'add_fragment' => '',     // Текст который добавиться ко всем ссылкам.
			'screen_reader_text' => __( 'Posts navigation' ),
			);
get_the_posts_pagination($args); 

function my_post_queries( $query ) {
  // do not alter the query on wp-admin pages and only alter it if it's the main query
  if (!is_admin() && $query->is_main_query()){
    // alter the query for the home and category pages 
    //$post_type = $query -> get('post_type');
    if( is_post_type_archive( 'boats' )){
      $query->set('posts_per_page', 2);
    }
  }
}
add_action( 'pre_get_posts', 'my_post_queries' );

function search_queries( $query ) {
  // do not alter the query on wp-admin pages and only alter it if it's the main query
  if (!is_admin() && $query->is_main_query()){
    // alter the query for the home and category pages 
    //$post_type = $query -> get('post_type');
    if( is_search( 'boats' )){
      $query->set('posts_per_page', 2);

    $caty = array(); 
	$tags = array();
	$prive = array();
	$group = array();
	$party = array();
	$price = array();
	$privateCharte="";
	$groupTrip ="";
	$partyBoardFishin ="";

	if (isset($_GET["Search"])) {
		# code...
		if (isset($_GET["term"])) 
		{
			$caty = "'".$_GET["term"]."'";
			if ($caty == "'None'"){
				$caty = array();
			}
			/*
			else
			{
				$caty = array(
	   				'taxonomy' => 'boat_type', 
	    			'field' => 'slug',
	    			'terms' => $caty,
	    		);
			}
			*/
		}
		if (isset($_GET["tagy"])) 
		{
			$tags = $_GET["tagy"];
			if ($tags[0] == "-"){
				$tags = array();
			}
			/*
			else{				
				$tags = array(
	    			'taxonomy' => 'boat_features', 
	    			'field' => 'slug',
	    			'terms' => $tags,
	    		);
			}
			*/			
		}

		if (isset($_GET["price"])) 
		{
			$price = $_GET["price"];
			if ($price!="None"){
				$price = array(
				'relation' => 'AND',
				array(
					'key' => 'price_range', 
					'value' => $price,
					'compare' => '<='
				),
				array(
					'key' => 'price_range', 
					'value' => $price-50,
					'compare' => '>='
				),
				);
			}
			else{
				$price = array();
			}
		}
		if (isset($_GET["privateCharter"])) 
		{
			$privateCharter = $_GET["privateCharter"];
			if ($privateCharter=="None"){
				$privateCharte ="Private Charter";
				$prive = array(
				'key' => 'checkbox', 
				'value' => $privateCharte,
				'compare' => 'LIKE'
				);
			}
		}
		if (isset($_GET["groupTrips"])) 
		{
			$groupTrips = $_GET["groupTrips"];
			if ($groupTrips=="None"){
				$groupTrip ="Group Trips";
				$group = array(
				'key' => 'checkbox', 
				'value' => $groupTrip,
				'compare' => 'LIKE'
				);
			}
		}
		if (isset($_GET["partyBoardFishing"])) 
		{
			$partyBoardFishing = $_GET["partyBoardFishing"];
			if ($partyBoardFishing=="None"){
				$partyBoardFishin ="Party Board Fishing";
				$party = array(
				'key' => 'checkbox', 
				'value' => $partyBoardFishin,
				'compare' => 'LIKE'
				);
			}
		}

		$query->set('boat_type', $caty);
		$query->set('boat_features', $tags);
	    $query->set('post_type','boats');
	    /*
	    if ( ( isset($_GET["tagy"]) ) && ( isset($_GET["term"]) ) ) {
	    	# code...
	    	$query->set('tax_query', 
	    	array(
	    		'relation' => 'AND',
	    		array(
		   			'taxonomy' => 'Boat_type', 
		    		'field' => 'slug',
		    		'terms' => array( $caty),
		    	),
	    		array(
	    			'relation' => 'or',
		    		array(
		    			'taxonomy' => 'Boat_features', 
		    			'field' => 'slug',
		    			'terms' => $tags,
		    		),
	    		), 
	    	)
		);
	    } else {
	    	if (isset($_GET["tagy"])) {
	    		$query->set('tax_query', 
			    	array(
			    			'taxonomy' => 'Boat_features', 
			    			'field' => 'slug',
			    			'terms' => $tags,
			    	)
				);
	    	} else {
	    		$query->set('tax_query', 
			    	array(
				    			'taxonomy' => 'Boat_type', 
				    			'field' => 'slug',
				    			'terms' => array( $caty),
			    	)
				);
	    		
	    	}
	    }
	    		



		$query->set('tax_query', 
	    	array(
	    		'relation' => 'or',
		    	$caty,
		    	$tags,
	    	)
		);
*/

	    $query->set('meta_query', 
	   		array(
				'relation' => 'or',
				$prive,
				$group,
				$party,
				$price,
			)
	   	);
		}		
    }
  }
}
add_action( 'pre_get_posts', 'search_queries' );


?>
<? $SearchProp = array(); ?>
<div class="search-banner-layer">
	<form action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<div class="container banner-content-area">
			<div class="heading-block">
				<h1>Lorem Ipsum is simply dummy text of the printing</h1>
				<h2>Lorem Ipsum is simply dummy text of the printing</h2>
			</div>
			<div class="category-checkboxes-block">
				<div class="squaredTwo">
					<input type="checkbox" value="None" id="privateCharter" name="privateCharter" <?php 
					if (isset($_GET["privateCharter"])) 
						{ 
							if ($_GET["privateCharter"]=="None") {
								array_push($SearchProp, "Private Charter");
								echo 'checked="checked"'; 
							}
						}
					?> />
					<label for="privateCharter">Private Charter</label>
				</div>
				<div class="squaredTwo">
					<input type="checkbox" value="None" id="groupTrips" name="groupTrips" <?php
					if (isset($_GET["groupTrips"])) 
						{ 
							if ($_GET["groupTrips"]=="None"){
								array_push($SearchProp, "Group Trips");
								echo 'checked="checked"'; 
							}
						}
					?> />
					<label for="groupTrips">Group Trips</label>
				</div>
				<div class="squaredTwo">
					<input type="checkbox" value="None" id="partyBoardFishing" name="partyBoardFishing" <?php 
					if (isset($_GET["partyBoardFishing"])) 
						{ 
							if ($_GET["partyBoardFishing"]=="None"){
								array_push($SearchProp, "Party Board Fishing");
								echo 'checked="checked"'; 
							}
						}
					?> />
					<label for="partyBoardFishing">Party Board Fishing</label>
				</div>
			</div>
			<div class="selects-area row">
				<div class="col-sm-3">
					<div class="castom-select">
						<select class="js-select-2" name="term">
							<option value="None">All select </option>
							<?php
								$terms = get_terms( 'Boat_type', 'orderby=count&hide_empty=0' );
								if( $terms && ! is_wp_error($terms) ){
									foreach( $terms as $term ){
										if (isset($_GET["term"])) 
										{
											if( $_GET["term"] == $term->name) { 
												echo '<option value="'.$term->name.'" selected="selected" >'. $term->name. "</option>";
												array_push($SearchProp, $_GET["term"]);
											}
											else{
												echo '<option value="'.$term->name.'" >'. $term->name. "</option>";
											//array_push($caty, $term->name);
											}
										}
										else{
											echo '<option value="'.$term->name.'" >'. $term->name. "</option>";
										}
									}
								}
							?>
						</select>
					</div>
				</div>
					
				<div class="col-sm-3">
					<div class="castom-select">
						<select class="js-select-2" multiple="true" name="tagy[]">
							<option value="-">All select </option>
							<?php
								$tag = get_terms( 'Boat_features', 'orderby=count&hide_empty=0' );
								if( $tag && ! is_wp_error($tag) ){
									foreach( $tag as $tagy ){
										$g=0;
										if (isset($_GET["tagy"])) 
										{
											$tags =  $_GET["tagy"];
											for ($i=0; $i < count($tags) ; $i++) { 
												# code...
												if( $tags[$i] == $tagy->name) { 
													echo '<option value="'.$tagy->name.'" selected="selected" >'. $tagy->name. "</option>";
													array_push($SearchProp, $tags[$i]);
													$g=1;
													break;
												}
											}
											if($g==0){
												echo '<option value="'.$tagy->name.'" >'. $tagy->name. "</option>";
											}
										}
										else{
											echo '<option value="'.$tagy->name.'" >'. $tagy->name. "</option>";
										}
									}
								}
							?>
						</select>
					</div>
				</div>
					
				<div class="col-sm-3">
					<div class="castom-select">
						<select class="js-select-2"  name="price">
							<option value="None">Select ALL</option>
							<?php
								$price = array( 100, 150, 200, 250, 300, 350);
								for ($i=0; $i < count($price) ; $i++) { 
									if( $_GET["price"] == $price[$i]) { 
										echo '<option value="'.$price[$i].'" selected="selected" >'. ($price[$i]-50)."-".$price[$i]. "</option>";
										array_push($SearchProp, ($price[$i]-50)."-".$price[$i]);
									}
									else{
										echo '<option value="'.$price[$i].'" >'. ($price[$i]-50)."-".$price[$i]. "</option>";
									//array_push($caty, $term->name);
									}
								}
							?>
						</select>
					</div>
				</div>						
						
				<div class="col-sm-3">
					<input type="hidden" name="s" />
					<input type="submit" class="wpm-btn btn" value="Search" name="Search" />Search</button>
				</div>
			</div>
		</div>
	</form>
</div>

<?php if (isset($_GET["Search"])) : ?>
<div>
	<section style="margin: 0 auto; width: 100;">
		<h1 style="text-align: center;">Поиск по следующип параметрам: 
			<?php 
			$trt=".";
			for ($i=0; $i < count($SearchProp) ; $i++) { 
				$trt = $trt." ".$SearchProp[$i].",";
			}
			echo substr($trt, 1, -1);
			/*
			print_r($_GET["tagy"]);
			print_r("'".$_GET["term"]."'");
			*/
			?>
		</h1>
	</section>
</div>
<? endif; ?>
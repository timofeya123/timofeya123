
<?php
get_header();
?>
	<div>
		<h3><? get_search_form();?></h3>
	</div>

<div>
	<section class="row recommence">
				<div class="col-md-12">
					<h1 style="text-align: center;">Все посты из метки: 
						<?php single_tag_title(); ?>
					</h1>
				</div>	
					<?	
					 	//$current_page = $_GET['page']; 
						$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
						//$paged = (get_query_var('page')) ? get_query_var('page') : 1;
						//$paged = get_query_var('paged') ? get_query_var('paged') : 1;

						$argss = array(
							'post_type' => 'boats',
							'publish' => true,
							'paged' => $paged,
							'posts_per_page' => 2,
							);
					
						$wp_query = new WP_Query($argss);

						if ( $wp_query->have_posts() ) {
						while ( $wp_query->have_posts() ) {
							$wp_query->the_post(); ?>
				<div class="col-md-4 col-sm-6">						
					<div class="recommence-card">
						<div class="recommence-card__content">
							<div class="recommence-card__mark">recommended</div>						
								<? the_post_thumbnail( $size= 'h-thumb'); ?>
							<div class="recommence-card__description">
								<div class="title">
									<? the_title();	?>
								</div>
								<p class="price"> from <span>
									<?	$value = get_field( "price_range" );

											if( $value ) {
											    
											    echo "$".$value;

											} else {

											    echo 'empty';
											    
											} ?>
								</span>per hour </p>
								<div class="options">
									<i class="icon-yacht"></i> <span>
									<? $value = get_field( "vessel_size" );

											if( $value ) {
											    
											    echo $value;

											} else {

											    echo 'empty';
											    
											} ?>
									 feet</span>
								</div>
								<div class="options">
									<i class="icon-person"></i> <span>
									<? $value = get_field( "vessel_occupancy" );

											if( $value ) {
											    
											    echo $value;

											} else {

											    echo 'empty';
											    
											} ?>
									 people</span>
								</div>
							</div>
						</div>				
					</div>
				</div>
				<?php
						}
						//$big = 999999999; // уникальное число

						//the_posts_pagination();

						wp_reset_postdata();

						/*// пагинация для произвольного запроса
						echo get_the_posts_pagination( array(
							'show_all'     => false, // показаны все страницы участвующие в пагинации
							'end_size'     => 1,     // количество страниц на концах
							'mid_size'     => 1,     // количество страниц вокруг текущей
							'prev_next'    => true,  // выводить ли боковые ссылки "предыдущая/следующая страница".
							'prev_text'    => __('« Previous'),
							'next_text'    => __('Next »'),
							'add_args'     => false, // Массив аргументов (переменных запроса), которые нужно добавить к ссылкам.
							'add_fragment' => '',     // Текст который добавиться ко всем ссылкам.
							'screen_reader_text' => __( 'Posts navigation' ),
						) );

						echo paginate_links( array(
							'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
							'format'  => '?paged=%#%',
							'current' => max( 1, get_query_var('paged') ),
							'total'   => $wp_query->max_num_pages,
							'prev_next'    => true,
						) );
						*/

					function my_pagenavi() {
						global $wp_query;
						$big = 999999999; // уникальное число для замены

						$args = array(
							'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
							'format' => '?paged=%#%',
							'current' => max( 1, get_query_var('paged') ),
							'total' => $wp_query->max_num_pages,
							'prev_next'    => true,
						);

						$result = paginate_links( $args );

						// удаляем добавку к пагинации для первой страницы
						$result = str_replace( '/page/1/', '', $result );
						echo $result; 
					}
				}
				//wp_reset_postdata();
				?>
			</section>
<div>
	<? 
		echo '<h3 style="text-align: center;">';
		my_pagenavi();
		echo "</h3>";
	?>
</div>
</div>

<?php
get_footer();
?>
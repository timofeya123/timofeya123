<?php get_header(); ?>

<? get_search_form(); ?> 

<div>
	<section class="row recommence" style="margin: 0 auto; width: 100;">
		<?php	
			if ( have_posts() ) :
			while ( have_posts() ) :
				the_post();
		?>
		<div class="col-md-4 col-sm-6" style="width: 50%; height: 20%;">						
			<div class="recommence-card">
				<div class="recommence-card__content">
					<div class="recommence-card__mark">recommended</div>						
						<? the_post_thumbnail( $size= 'h-thumb'); ?>
					<div class="recommence-card__description">
					<div class="title">
						<? the_title();	?>
					</div>
						<p class="price"> from <span>
							<?php	
								$value = get_field( "price_range" );
								if( $value ) {
								    echo "$".$value;
								} else {
								    echo 'empty';
								} 
							?>
						</span>per hour </p>
					<div class="options">
						<i class="icon-yacht"></i> <span>
							<?php
								$value = get_field( "vessel_size" );
								if( $value ) {
								   echo $value;
								} else {
								    echo 'empty';
								} 
							?>
						 feet</span>
					</div>
					<div class="options">
						<i class="icon-person"></i> <span>
							<?php 
								$value = get_field( "vessel_occupancy" );
								if( $value ) {
								    echo $value;
								} else {
								    echo 'empty';
								} 
							?>
						people</span>
					</div>
				</div>
			</div>				
		</div>
	</div>
	<?php
			endwhile;	
			wp_reset_postdata();
		endif;
	?>
</section>
<div>
	<?php 
		echo '<h3 style="text-align: center;">';
		the_posts_pagination();
		echo "</h3>";
	?>
</div>
</div>

<?php
get_footer();
?>
